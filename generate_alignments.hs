import PGF
import Data.Maybe (fromJust)
import System.Console.GetOpt
import System.Environment (getArgs)
import System.Exit (exitFailure)
import Control.Monad (unless)
import Data.List (intercalate)

description = unlines ["usage: generate_alignments [--cat STARTCAT] [--depth MAXDEPTH] PGF"]

data Options = Options  { optCat :: String, optDepth :: Int }

startOptions :: Options
startOptions = Options  { optCat = "Utt", optDepth = 3 }

options :: [ OptDescr (Options -> Options) ]
options =
  [ Option "c" ["cat"]
      (ReqArg (\arg opt -> opt { optCat = arg }) "CATEGORY") $
      "Start category (default '" ++ optCat startOptions ++ "')"
  , Option "d" ["depth"]
      (ReqArg (\arg opt -> opt { optDepth = read arg }) "DEPTH") $
      "Max depth (default " ++ show (optDepth startOptions) ++ ")" ]

main = do
  args <- getArgs
  let (actions, nonOptions, errors) = getOpt Permute options args
  unless (length nonOptions == 1) $ do
    putStrLn $ usageInfo description options
    exitFailure
  let opts = foldl (flip ($)) startOptions actions
  pgf <- readPGF (head nonOptions)
  let myType = fromJust $ readType (optCat opts)
  let alignments = generateAlignments pgf myType (optDepth opts)
  putStrLn $ unlines $ map (intercalate " | ") alignments

type CSV = [[String]]

generateAlignments :: PGF -> Type -> Int -> CSV
generateAlignments pgf startCat depth = map align trees
  where align tree =
          let count = 1
          in case gizaAlignment pgf (lang1,lang2) tree of
            ([],_,_) -> fail $ "Cannot linearize in English: " ++ show tree
            (_,[],_) -> fail $ "Cannot linearize in Spanish: " ++ show tree
            (s1,s2,a) -> [show count, s1, s2, replace '-' ':' a]
        [lang1,lang2] = languages pgf
        trees = generateAllDepth pgf startCat (Just depth)

replace :: (Eq a) => a -> a -> [a] -> [a]
replace a a' as = map replace' as
  where replace' x | x == a = a'
        replace' x | otherwise = x
