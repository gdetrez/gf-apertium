import Test.Hspec
import Test.QuickCheck
import Control.Exception (evaluate)
import Control.Monad (unless)
import PGF
import Text.Regex.TDFA

regex = "^\\^.*(<[a-z]*>)+\\$$"

-- helper functions
shouldMatch, shouldNotMatch :: String -> String -> Expectation
shouldMatch s p = (s =~ p) `shouldBe` True
shouldNotMatch s p = (s =~ p) `shouldBe` False

hasFunction :: PGF -> String -> Expectation
hasFunction pgf f = elem (mkCId f) (functions pgf) `shouldBe` True

main :: IO ()
main = do
  pgf <- readPGF "./ExampleGenerator.pgf"
  hspec $ do
    describe "Apertium regex" $ do

      it "should not accept the empty string" $
        "" `shouldNotMatch` regex

      it "should not accept a simple word" $
        "hello" `shouldNotMatch` regex

      it "should accept a tagged word" $
        "^prpers<prn><subj><p3><nt><sg>$" `shouldMatch` regex

    let funs =  [ "UseN", "DetQuant", "DetCN", "NumSg", "PPos", "ASimul"
                , "PrepNP", "TTAnt", "IndefArt", "UseCl", "PredVP"
                , "TPres", "DefArt", "NumPl", "AdjCN", "PositA", "PhrUtt"
                , "NoVoc", "UttS", "NoPConj", "SlashV2a" {-"MkSymb", "SymbPN"-}
                , "UsePN", {-"ComplSlash",-} "AdvNP", "MassNP", {-"CompoundCN",-} "AdvVP"
                , "TPast", "UsePron", "in_Prep", "and_Conj", "ComplVS", "PossNP"
                , {- "IIDig",-} "UseComp", "UseV", "it_Pron", {-"NumCard",-} "PossPron"
                , "for_Prep", "ComplVV", "ConjNP", "BaseNP", {-"say_VS",-} "to_Prep"
                , "AdvVPSlash", {- "of_Prep",-} {-"GenNP", "UseQuantPN",-} "RelNP", "AdVVP"
                , "he_Pron", "DetNP", "UseRCl", {-"IDig", "NumDigits",-} "on_Prep"
                , "they_Pron", {-"by_Prep", "at_Prep",-} "RelVP", "CompNP"
                , "from_Prep", {-"PassVPSlash",-} "with_Prep" {-"PositAdVAdj"-}
                , "CompAP", "IdRP", "AdvS" {-"as_Prep", "ApposNP", "year_N"-}
                , {-"D_0", "company_N", "D_1"-} "PositAdvAdj" {-"AAnter", "D_9"-}
                {-"GerundN", "pot2as3", "pot1as2", "num", "pot0as1", "PastPartAP"-}
                , "ProgrVP", {-"share_N", "market_N", "NumNumeral",-} "or_Conj"
                , "this_Quant", {-"that_RP"-} "PNeg" {-"pot0" "stock_N" "can_VV"-}
                , "we_Pron" {-"D_8", "have_V2" -}
                , "aFeminineNoun", "aMasculineNoun"
                ]
    describe "functions" $ flip mapM_ funs $ \fun -> do
      describe fun $ do
        it "should exists in the abstract grammar" $
          pgf `hasFunction` fun
        it "linearize in the apertium stream format (English)" $
          let tokens = getTokenForFunction pgf english fun in
          flip mapM_ tokens (\a ->
            a `shouldSatisfy` (\(t,_) -> (t =~ regex || t == "&+")))
        it "linearize in the apertium stream format (Spanish)" $
          let tokens = getTokenForFunction pgf spanish fun in
          flip mapM_ tokens (\a ->
            a `shouldSatisfy` (\(t,_) -> (t =~ regex || t == "&+")))

  where Just english = readLanguage "ExampleGeneratorEng"
        Just spanish = readLanguage "ExampleGeneratorSpa"


getTokenForFunction :: PGF -> Language -> String -> [(String, Analysis)]
getTokenForFunction pgf lang fun =
  [ (token, params) | (token, analyses) <-  fflex ,
                      (fun',params)     <- analyses ,
                      fun' == cid ]
  where cid = mkCId fun
        fflex = fullFormLexicon (buildMorpho pgf lang)
