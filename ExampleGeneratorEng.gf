concrete ExampleGeneratorEng of ExampleGenerator =
  CatEng,
  ConjunctionEng [ListNP],
  GrammarEng [ UseN, DetQuant, DetCN, NumSg, PPos, ASimul
    , PrepNP, TTAnt, {- IndefArt, -} UseCl, PredVP
    , TPres, {- DefArt, -} NumPl, AdjCN, PositA, PhrUtt
    , NoVoc, UttS, NoPConj , SlashV2a, {-MkSymb,-} SymbPN
    , UsePN, CompSlash, AdvNP, MassNP, CompoundCN, AdvVP
    , TPast, UsePron {-in_Prep,-} {-and_Conj-} {-ComplVS,-} {-PossNP-}
    , {-IIDig-} {-UseComp-} UseV, {- it_Pron -} PossPron
    , {- for_Prep -} ComplVV, ConjNP, BaseNP, say_VS {- to_Prep -}
    , AdvVPSlash, of_Prep, GenNP, UseQuantPN, AdVVP
    , {-he_Pron,-} DetNP, UseRCl {-on_Prep-}
    , {-they_Pron,-} RelVP, CompNP
    {-from_Prep, with_Prep,-}
    , CompAP, {- IdRP,-} AdvS, PositAdvAdj
    , GerundN, PastPartAP, ProgrVP, share_N, market_N {-or_Conj-}
    , {-this_Quant,-} that_RP, PNeg, stock_N, can_VV
    , {-we_Pron, D_8,-} have_V2n
    -- Utterances
    , UttS       -- John walks
    , UttQS      -- is it good
    , UttImpSg   -- (don't) love yourself
    , UttImpPl   -- (don't) love yourselves
    , UttImpPol  -- (don't) sleep (polite)
    , UttIP      -- who
    , UttIAdv    -- why
    , UttNP      -- this man
    , UttAdv     -- here
    , UttVP      -- to sleep
    , UttCN      -- house
    , UttCard    -- five
    , UttAP      -- fine
    , UttInterj  -- alas
    -- Cats
    , A , AP , AdV , Adv , Ant
    , CN , Card , Cl , Comp , Conj
    , Det
    , IAdv , IP , Imp
    , ListNP
    , N , NP , Num
    , PConj , PN , Phr , Pol , Prep , Pron
    , QS , Quant
    , RCl , RClTense , RP , RS
    , S
    , Temp , Tense
    , Utt
    , V , VP , VV , VV , Voc
  ] ** open ResEng, ApertiumStreamFormat, Prelude in {

  -- ~~~ IndefArt ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin
    IndefArt = {
      s = \\hasCard,n => case <n,hasCard> of {
        <Sg,False> => mkLu "a" "det" "ind" "sg" ;
        _          => []
      } ;
      sp = \\hasCard,n,case_ => case <hasCard,case_> of {
        < False, NCase  Gen > => mkGenitive (one_prn!n) ;
        < False, _ >          => one_prn!n ;
        < True, _ >           => []
      }
    } ;

  -- ~~~ DefArt ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin DefArt = {
    s  = \\hasCard,n => mkLu "the" "det" "def" "sp";
    sp = \\hasCard,n,c => case <hasCard,n,c> of {
      <True, Sg, NCase Nom>   => mkLu "the" "det" "def" "sp" ; -- the
      <True, Sg, NCase Gen>   => mkLu "the" "det" "def" "sp" ; -- the
      <True, Sg, NPAcc>       => mkLu "the" "det" "def" "sp" ; -- the
      <True, Sg, NPNomPoss>   => mkLu "the" "det" "def" "sp" ; -- the
      <True, Pl, NCase Nom>   => mkLu "the" "det" "def" "sp" ; -- the
      <True, Pl, NCase Gen>   => mkLu "the" "det" "def" "sp" ; -- the
      <True, Pl, NPAcc>       => mkLu "the" "det" "def" "sp" ; -- the
      <True, Pl, NPNomPoss>   => mkLu "the" "det" "def" "sp" ; -- the
      <False, Sg, NCase Nom>  => mkLu "prpers" "prn" "subj" "p3" "nt" "sg" ; -- it
      <False, Sg, NCase Gen>  => mkLu "its" "det" "pos" "sp" ; -- its
      <False, Sg, NPAcc>      => mkLu "prpers" "prn" "obj" "p3" "nt" "sg" ;-- it
      <False, Sg, NPNomPoss>  => mkLu "prpers" "prn" "subj" "p3" "nt" "sg" ; -- it
      <False, Pl, NCase Nom>  => mkLu "their" "det" "pos" "sp" ; -- their
      <False, Pl, NCase Gen>  => mkLu "theirs" "prn" "pos" "mf" "sp" ; -- theirs
      <False, Pl, NPAcc>      => mkLu "prpers" "prn" "obj" "p3" "mf" "pl" ; -- them
      <False, Pl, NPNomPoss>  => mkLu "theirs" "prn" "pos" "mf" "sp" -- theirs
    }
  } ;

  oper predVc : (Verb ** {c2 : Str}) -> SlashVP = \verb -> 
    predV verb ** {c2 = verb.c2 ; gapInMiddle = True} ;

  oper not : Str = mkLu "not" "adv" ;

  oper of_pr = mkLu "of" "pr";
--     comma_cm = mkLu "," "cm" ;
    mkGenitive : Str -> Str
      = \s -> s ++ mkLu "'s" "gen" ;
    one_prn = table {
      Sg => mkLu "one" "prn" "tn" "mf" "sg" ;
      Pl => mkLu "one" "prn" "tn" "mf" "pl"
    } ;

  -- Exampleis
  lincat Example = { s : Str} ;
  lin ExampleNP np = { s = np.s ! (NCase Nom) } ;

  -- lexicon

--   -- --- Adverbs ------------------------------------------------------------
--   oper mkAdv : Str -> Adv = \s -> lin Adv { s = mkLu s "adv" } ;
--   lin
--     somewhere_Adv   = mkAdv "somewhere" ;
--     everywhere_Adv  = mkAdv "everywhere" ;
--     nowhere_Adv     = mkAdv "nowhere" ;
--     here7from_Adv   = mkAdv "from here" ;
--     here7to_Adv     = { s = to_pr ++ mkLu "here" "adv"} ;
--     always_AdV      = mkAdv "always" ;
--   -- CAdv: comparative adverb
--   lin
--     as_CAdv   = { s = mkLu "as" "cnjadv" ; p = mkLu "as" "cnjadv" } ;
--     less_CAdv = { s = mkLu "less" "adv" ; p = mkLu "than" "cnjcoo" } ;
--     more_CAdv = { s = mkLu "more" "adv" ; p = mkLu "than" "cnjcoo" } ;
--   -- AdA: adjective-modifying adverb e.g. "very"
--   oper mkAdA : Str -> Adv = \s -> lin Adv { s = mkLu s "preadv" } ;
--   lin
--     almost_AdA = { s = mkLu "almost" "adv"} ;
--     quite_Adv = mkAdA "quite" ;
--     so_AdA = mkAdA "so" ;
--     too_AdA = mkAdA "too" ;
--     very_AdA = mkAdA "very" ;
-- 
--   lin
--     at_most_AdN = { s = mkLu "at_most" "adv" } ;
--     almost_AdN  = { s = mkLu "almost" "adv" } ;
--   oper not : Str = mkLu "not" "adv" ;
-- 
--   -- --- Conjunctions --------------------------------------------------------
--   --
--   lin
--     either7or_DConj = { s1 = mkLu "either" "adv" ;
--                         s2 = mkLu "or" "cnjcoo" ;
--                         n  = Sg } ;
  lin or_Conj         = { s1 = [] ;
                          s2 = mkLu "or" "cnjcoo" ;
                          n = Sg };
--     and_Conj        = { s1 = [] ;
--                         s2 = mkLu "and" "cnjcoo" ;
--                         n = Pl };
--     both7and_DConj  = { s1 = mkLu "both" "cnjcoo"  ;
--                         s2 = mkLu "and" "cnjcoo" ;
--                         n = Pl };
--     if_then_Conj    = { s1 = mkLu "if" "cnjadv"  ;
--                         s2 = mkLu "then" "cnjadv" ;
--                         n = Sg };
-- 
--   -- --- Clauses ----------------------------------------------------------
--   lin
--     QuestCl cl = {
--       s = \\t,a,p =>
--             let cls = cl.s ! t ! a ! p
--             in table {
--               QDir   => cls ! OQuest ;
--               QIndir => "if" ++ cls ! ODir
--             }};
-- 
--   -- --- Determiners --------------------------------------------------------
--   -- lincat in english/CatEng.gf:
--   -- Det = {s : Str ; sp : NPCase => Str ; n : Number ; hasNum : Bool} ;
--   --
--   -- s is the determiner form
--   -- sp is the form used as a pronouns?
--   -- n the number of the np
--   -- hasNum ??
-- 
--   lin few_Det = {
--     s = mkLu "few" "det" "qnt" "pl" ;
--     sp = table {
--       NCase Gen => mkGenitive (mkLu "few" "prn" "tn" "mf" "pl") ;
--       _         => mkLu "few" "prn" "tn" "mf" "pl"
--     } ;
--     n = Pl ;
--     hasNum = False
--   } ;
-- 
--   much_Det = {
--     s = mkLu "much" "det" "qnt" "sg" ;
--     sp = table {
--       NCase Gen => mkGenitive (mkLu "much" "adv") ;
--       _         => mkLu "much" "adv"
--     } ;
--     n = Pl ;
--     hasNum = False
--   } ;
-- 
--   lin every_Det = {
--     s= mkLu "every" "det" "ind" "sg" ;
--     sp = table {
--       NCase Gen => mkGenitive (mkLu "everyone" "prn" "tn" "m" "sg") ;
--       _         => mkLu "everyone" "prn" "tn" "m" "sg"
--     } ;
--     n = Pl ;
--     hasNum = False
-- 
--   } ;
-- 
--   many_Det = {
--     s= mkLu "every" "det" "qnt" "pl" ;
--     sp = table {
--       NCase Gen => mkGenitive (mkLu "many" "prn" "tn" "mf" "pl") ;
--       _         => mkLu "many" "prn" "tn" "mf" "pl"
--     } ;
--     n = Pl ;
--     hasNum = False
--   } ;
-- 
--   someSg_Det = {
--     s= mkLu "some" "det" "qnt" "sp" ;
--     sp = let prn : Str = mkLu "some" "prn" "tn" "mf" "pl"
--       in table { NCase Gen => mkGenitive prn ; _ => prn } ;
--     n = Sg ;
--     hasNum = False
--   } ;
-- 
--   somePl_Det = {
--     s= mkLu "some" "det" "qnt" "sp" ;
--     sp = let prn : Str = mkLu "some" "prn" "tn" "mf" "pl"
--       in table { NCase Gen => mkGenitive prn ; _ => prn } ;
--     n = Pl ;
--     hasNum = False
--   } ;
-- 
--   -- --- Idioms ------------------------------------------------------------
 
--   lin
--   ImpersCl vp = mkClause it_prpers (agrP3 Sg) vp ;
--   GenericCl vp = mkClause (mkLu "one" "prn" "tn" "mf" "sg") (agrP3 Sg) vp ;
--   CleftNP np rs = mkClause it_prpers (agrP3 Sg)
--     (insertObj (\\_ => rs.s ! np.a)
--       (insertObj (\\_ => np.s ! rs.c) (predAux ExampleGeneratorEng.auxBe))) ;
-- 
--   CleftAdv ad s = mkClause it_prpers (agrP3 Sg)
--     (insertObj (\\_ => conjThat ++ s.s)
--       (insertObj (\\_ => ad.s) (predAux ExampleGeneratorEng.auxBe))) ;
-- 
--   ExistNP np =
--     mkClause "there" (agrP3 (fromAgr np.a).n)
--       (insertObj (\\_ => np.s ! NPAcc) (predAux ExampleGeneratorEng.auxBe)) ;
-- 
--   ExistIP ip =
--     mkQuestion (ss (ip.s ! npNom))
--       (mkClause "there" (agrP3 ip.n) (predAux ExampleGeneratorEng.auxBe)) ;
-- 
--   ExistNPAdv np adv =
--     mkClause "there" (agrP3 (fromAgr np.a).n)
--       (insertObj (\\_ => np.s ! NPAcc ++ adv.s)
--         (predAux ExampleGeneratorEng.auxBe)) ;
-- 
--   ExistIPAdv ip adv =
--     mkQuestion (ss (ip.s ! npNom))
--       (mkClause "there" (agrP3 ip.n) (insertObj (\\_ => adv.s)
--         (predAux ExampleGeneratorEng.auxBe))) ;
-- 
--   ProgrVP vp = insertObj (\\a => vp.ad ++ vp.prp ++ vp.s2 ! a)
--     (predAux ExampleGeneratorEng.auxBe) ;
--   ImpPl1 vp = {s = "let's" ++ infVP VVAux vp Simul CPos (AgP1 Pl)} ;
--   ImpP3 np vp = {s = "let" ++ np.s ! NPAcc ++ infVP VVAux vp Simul CPos np.a} ;
-- 
--   -- --- Pre-determiners ----------------------------------------------------
--   lin
--     all_Predet  = { s = mkLu "all"  "predet" "sp" } ;
--     most_Predet = { s = mkLu "most" "det" "qnt" "sp" } ;
--     only_Predet = { s = mkLu "only" "adv" } ;
--     not_Predet  = { s = not } ;
-- 
  -- --- Prepositions -------------------------------------------------------
  lin
    above_Prep    = mkPrep "above" ;
    after_Prep    = mkPrep "after" ;
    before_Prep   = mkPrep "before" ;
    behind_Prep   = mkPrep "behind" ;
    between_Prep  = mkPrep "between" ;
    by8agent_Prep = mkPrep "by" ;
    by8means_Prep = mkPrep "by" ;
    during_Prep   = mkPrep "during" ;
    except_Prep   = mkPrep "except" ;
    for_Prep      = mkPrep "for" ;
    from_Prep     = mkPrep "from" ;
    in8front_Prep = mkPrep "in front of" ;
    in_Prep       = mkPrep "in" ;
    on_Prep       = mkPrep "on" ;
    part_Prep     = mkPrep "of" ;
    possess_Prep  = mkPrep "of" ;
    through_Prep  = mkPrep "through" ;
    under_Prep    = mkPrep "under" ;
    with_Prep     = mkPrep "with" ;
    without_Prep  = mkPrep "without" ;

  -- --- Pronouns -----------------------------------------------------------

  -- Function that modify the strings of a pronoun:
  --
  -- The arguments are named according to their tags in apertium:
  --  prn_subj: personal pronoun subject
  --  prn_obj: personal pronoun object
  --  det_pos: possessive determiner
  --  prn_pos: possessive pronoun
  oper mkNewPron : Pron -> (s1,s2,s3,s4 : Str) -> Pron
    = \pron, prn_subj, prn_obj, det_pos, prn_pos -> lin Pron
    { s = table {
        NCase Nom => prn_subj ;
        NCase Gen => det_pos ;
        NPAcc     => prn_obj ;
        NPNomPoss => prn_pos } ;
      sp = table Case [ prn_pos; mkGenitive prn_pos ] ;
      a  = pron.a } ;

--   -- Pronouns in English have the following type in english/CatEng.gf:
--   -- Pron = {s : NPCase => Str ; sp : Case => Str ; a : Agr} ;
--   --
--   -- s  : is the pronoun form
--   -- sp : is
--   -- a  : is the aggrement information (gender and number)
--   i_Pron = {
--     s  = table {
--       NCase Nom => mkLu "prpers" "prn" "subj" "p1" "mf" "sg" ;
--       NCase Gen => mkLu "my" "dt" "pos" "sp" ;
--       NPAcc     => mkLu "prpers" "prn" "obj" "p1" "mf" "sg" ;
--       NPNomPoss => mkLu "mine" "prn" "pos" "mf" "sp"
--     } ;
--     sp = GrammarEng.i_Pron.sp ;
--     a  = GrammarEng.i_Pron.a
--   } ;
--   youSg_Pron = {
--     s  = table {
--       NCase Nom => mkLu "prpers" "prn" "subj" "p2" "mf" "sp" ;
--       NCase Gen => mkLu "your" "det" "pos" "sp" ;
--       NPAcc     => mkLu "prpers" "prn" "obj" "p2" "mf" "sp" ;
--       NPNomPoss => mkLu "yours" "prn" "pos" "mf" "sp"
--     } ;
--     sp = GrammarEng.youSg_Pron.sp ;
--     a  = GrammarEng.youSg_Pron.a
--   } ;
--   youPl_Pron = {
--     s  = ExampleGeneratorEng.youSg_Pron.s ;
--     sp = ExampleGeneratorEng.youSg_Pron.sp ;
--     a  = GrammarEng.youPl_Pron.a
--   } ;
--   youPol_Pron = {
--     s  = ExampleGeneratorEng.youSg_Pron.s ;
--     sp = ExampleGeneratorEng.youSg_Pron.sp ;
--     a  = GrammarEng.youPol_Pron.a
--   } ;
-- 
--   she_Pron = {
--     s  = table {
--       NCase Nom => mkLu "prpers" "prn" "subj" "p3" "f" "sg" ;
--       NCase Gen => mkLu "her" "det" "pos" "sp" ;
--       NPAcc     => mkLu "prpers" "prn" "obj" "p3" "f" "sg" ;
--       NPNomPoss => mkLu "hers" "prn" "pos" "mf" "sp"
--     } ;
--     sp = GrammarEng.she_Pron.sp ;
--     a  = GrammarEng.she_Pron.a
--   } ;

  lin he_Pron = mkNewPron GrammarEng.he_Pron
                (mkLu "prpers" "prn" "subj" "p3" "m" "sg")
                (mkLu "prpers" "prn" "obj" "p3" "m" "sg")
                (mkLu "his" "det" "pos" "sp")
                (mkLu "his" "prn" "pos" "mf" "sp") ;



  lin it_Pron = mkNewPron GrammarEng.it_Pron
                (mkLu "prpers" "prn" "subj" "p3" "nt" "sg")
                (mkLu "prpers" "prn" "obj" "p3" "nt" "sg")
                (mkLu "its" "det" "pos" "sp")
                (mkLu "its" "det" "pos" "sp") ;
  lin we_Pron = mkNewPron GrammarEng.we_Pron
      (mkLu "prpers" "prn" "subj" "p1" "mf" "pl")
      (mkLu "prpers" "prn" "obj" "p1" "mf" "pl")
      (mkLu "our" "det" "pos" "sp")
      (mkLu "ours" "prn" "pos" "mf" "sp") ;

  lin they_Pron = mkNewPron GrammarEng.they_Pron
      (mkLu "prpers" "prn" "subj" "p3" "mf" "pl")
      (mkLu "prpers" "prn" "obj" "p3" "mf" "pl")
      (mkLu "their" "det" "pos" "sp")
      (mkLu "theirs" "prn" "pos" "mf" "sp") ;

--   -- --- Punctuation ------------------------------------------------------
--   lin
--     TEmpty          = {s = []} ;
--     TFullStop x xs  = {s = x.s ++ mkLu "." "sent" ++ xs.s} ;
--     TQuestMark x xs = {s = x.s ++ mkLu "?" "sent" ++ xs.s} ;
--     TExclMark x xs  = {s = x.s ++ mkLu "!" "sent" ++ xs.s} ;
-- 
  -- --- Quantifiers --------------------------------------------------------
  -- Some determiners, like this, have cat Quant with the following lincat
  -- Quant = {s : Bool => Number => Str ; sp : Bool => Number => NPCase => Str} ;
  --
  -- s depends on a Boolean which says if the Quant is combined with a numeral
  -- sp is the form used when no noun is present, as in DetNP
  lin this_Quant = {
    s = \\_ => table {
      Sg => mkLu "this" "det" "dem" "sg" ;
      Pl => mkLu "this" "det" "dem" "pl" } ;
    sp = \\_ => table {
      Sg => table {
        NCase Gen => mkGenitive (mkLu "this" "prn" "tn" "mf" "sg") ;
        _         => mkLu "this" "prn" "tn" "mf" "sg"} ;
      Pl => table {
        NCase Gen => mkGenitive (mkLu "this" "prn" "tn" "mf" "pl") ;
        _         => mkLu "this" "prn" "tn" "mf" "pl" }
    }
  } ;


--   lin that_Quant = {
--     s = \\_ => table {
--       Sg => mkLu "that" "det" "dem" "sg" ;
--       Pl => mkLu "that" "det" "dem" "pl" } ;
--     sp = \\_ => table {
--       Sg => table {
--         NCase Gen => mkGenitive (mkLu "that" "prn" "tn" "mf" "sg") ;
--         _         => mkLu "that" "prn" "tn" "mf" "sg"} ;
--       Pl => table {
--         NCase Gen => mkGenitive (mkLu "that" "prn" "tn" "mf" "pl") ;
--         _         => mkLu "that" "prn" "tn" "mf" "pl" }
--     }
--   } ;
-- 
--   lin no_Quant = {
--     s = \\_,_ => mkLu "no" "det" "ind" "sp" ;
--     sp = \\_,_ => table {
--       NCase Gen => mkGenitive (mkLu "none" "prn" "tn" "nt" "sg") ;
--       _         => mkLu "none" "prn" "tn" "nt" "sg"
--     }
--   };
-- 
--   -- --- Relative Pronouns ------------------------------------------------
--   -- RP = {s : RCase => Str ; a : RAgr} ;
--   lin IdRP =
--     { s = table {
--         RC _ (NCase Gen) | RC _ NPNomPoss => mkLu "whose" "rel" "aa" "mf" "sp";
--         RC Neutr _  => mkLu "which" "rel" "an" "mf" "sp" ;
--         RC _ NPAcc    => mkLu "whom" "rel" "an" "mf" "sp" ;
--         RC _ (NCase Nom)    => mkLu "who" "rel" "an" "mf" "sp" ;
--         RPrep Neutr => mkLu "which" "rel" "an" "mf" "sp" ;
--         RPrep _     => mkLu "whom" "rel" "an" "mf" "sp" };
--       a = GrammarEng.IdRP.a
--     } ;
-- 
--   -- --- Subjunction ------------------------------------------------------
--   lin
--     when_Subj = { s = mkLu "when" "cnjadv" };
-- 
--   -- --- Utt --------------------------------------------------------------
--   lin no_Utt = { s = mkLu "no" "adv" };
--       yes_Utt = { s = mkLu "yes" "adv" };
--   -- --- Verbs ------------------------------------------------------------
--   -- VV = {s : VVForm => Str ; p : Str ; typ : VVType} ;
--   -- VVForm are in order: infinitive, pres 3rd person, part, -ing, past,
--   --                      VVPresNeg, VVPastNeg
--   lin
--     want_VV = {
--       s = table {
--         VVF VInf      => mkLu "want" "vblex" "inf" ;
--         VVF VPres     => mkLu "want" "vblex" "pri" "p3" "sg" ;
--         VVF VPPart    => mkLu "want" "vblex" "pp" ;
--         VVF VPresPart => mkLu "want" "vblex" "pprs" ;
--         VVF VPast     => mkLu "want" "vblex" "past" ;
--         VVPresNeg     => mkLu "want" "vblex" "inf" ;
--         VVPastNeg     => mkLu "want" "vblex" "inf" };
--       p = GrammarEng.want_VV.p ;
--       typ = GrammarEng.want_VV.typ
--     };
-- 
--     can_VV = {
--       s = table {
--         VVF VInf      => mkLu "be able to" "vbmod" "inf"  ; -- be able to
--         VVF VPres     => mkLu "can" "vaux" "pres"         ; -- can
--         VVF VPPart    => mkLu "be able to" "vbmod" "pp"   ; -- been able to
--         VVF VPresPart => mkLu "be able to" "vbmod" "pprs" ; -- being able to
--         VVF VPast     => mkLu "can" "vaux" "past"         ; -- could
--         VVPresNeg     => mkLu "can" "vaux" "pres" + not   ; -- can't
--         VVPastNeg     => mkLu "can" "vaux" "past" + not   }; -- couldn't
--       p = GrammarEng.want_VV.p ;
--       typ = GrammarEng.want_VV.typ
--     };
--     can8know_VV = ExampleGeneratorEng.can_VV;
-- 
--     must_VV = {
--       s = table {
--         VVF VInf      => mkLu "have# to" "vbmod" "inf"  ; -- have to
--         VVF VPres     => mkLu "must" "vaux" "inf"       ; -- must
--         VVF VPPart    => mkLu "have# to" "vbmod" "pp"   ; -- had to
--         VVF VPresPart => mkLu "have# to" "vbmod" "pprs" ; -- having to
--         VVF VPast     => mkLu "have# to" "vbmod" "past" ; -- had to
--         VVPresNeg     => mkLu "must" "vaux" "inf" ++ not; -- musn't
--         VVPastNeg     => mkLu "have" "vbhaver" "past"
--                       ++ not ++ mkLu "to" "pr"           }; -- hadn't to
--       p = GrammarEng.want_VV.p ;
--       typ = GrammarEng.want_VV.typ
--     };
-- 
   --- Nouns --------------------------------------------------------------
  lin
    aFeminineNoun = {
      s = \\n,g => case <n,g> of {
        <Sg, Nom> => mkLu "house" "n" "sg" ;
        <Sg, Gen> => mkLu "house" "n" "sg" ++  mkLu "'s" "gen";
        <Pl, Nom> => mkLu "house" "n" "pl" ;
        <Pl, Gen> => mkLu "house" "n" "pl" ++  mkLu "'s" "gen" };
      g = Neutr };
    aMasculineNoun = {
      s = \\n,g => case <n,g> of {
        <Sg, Nom> => mkLu "car" "n" "sg" ;
        <Sg, Gen> => mkLu "car" "n" "sg" ++  mkLu "'s" "gen" ;
        <Pl, Nom> => mkLu "car" "n" "pl" ;
        <Pl, Gen> => mkLu "car" "n" "pl" ++  mkLu "'s" "gen" };
      g = Neutr };
-- 
-- 
--   -- Noun phrase
--   oper mkNP_ : Str -> Agr -> NP
--     = \somebody,agr -> lin NP {
--         s = table {
--           NCase Nom => somebody ;
--           NCase Gen => mkGenitive somebody ;
--           NPAcc     => somebody ;
--           NPNomPoss => mkGenitive somebody
--           } ;
--         a = agr
--       };
--   lin
--     somebody_NP   = mkNP_ (mkLu "somebody" "prn" "tn" "m" "sg") (AgP3Sg Neutr) ;
--     everybody_NP  = mkNP_ (mkLu "everybody" "prn" "tn" "m" "sg") (AgP3Sg Neutr) ;
--     nobody_NP     = mkNP_ (mkLu "nobody" "prn" "tn" "m" "sg") (AgP3Sg Neutr) ;
--     something_NP  = mkNP_ (mkLu "something" "prn" "tn" "nt" "sg") (AgP3Sg Neutr) ;
--     everything_NP = mkNP_ (mkLu "everything" "prn" "tn" "m" "sg") (AgP3Sg Neutr) ;
--     nothing_NP    = mkNP_ (mkLu "nothing" "prn" "tn" "m" "sg") (AgP3Sg Neutr) ;
-- 
-- 
--   -- Prepositions
--   oper
--     from_pr : Str = mkLu "from" "pr" ;
--     to_pr   : Str = mkLu "to" "pr" ;
--   -- Adverbs
--   lin
--     there_Adv = { s = mkLu "there" "adv" } ;
--     there7to_Adv   = ExampleGeneratorEng.there_Adv ;
--     there7from_Adv = { s = from_pr ++ ExampleGeneratorEng.there_Adv.s } ;
--     here_Adv = { s = mkLu "here" "adv" } ;
-- 
  -- Adjectives
  lin short_A =
      let short   : Str = mkLu "short" "adj" "sint" ;
          shorter : Str = mkLu "short" "adj" "sint" "comp" ;
          shortest: Str = mkLu "short" "adj" "sint" "sup"
      in {
        s = table {
          AAdj Posit Nom  => short ;
          AAdj Posit Gen  => mkGenitive short ;
          AAdj Compar Nom => shorter ;
          AAdj Compar Gen => mkGenitive shorter ;
          AAdj Superl Nom => shortest ;
          AAdj Superl Gen => mkGenitive shortest ;
          AAdv            => mkLu "shortly" "adv"
        }
     } ;
  lin new_A =
      let new   : Str = mkLu "new" "adj" "sint" ;
          newer : Str = mkLu "new" "adj" "sint" "comp" ;
          newest: Str = mkLu "new" "adj" "sint" "sup"
      in {
        s = table {
          AAdj Posit Nom  => new ;
          AAdj Posit Gen  => mkGenitive new ;
          AAdj Compar Nom => newer ;
          AAdj Compar Gen => mkGenitive newer ;
          AAdj Superl Nom => newest ;
          AAdj Superl Gen => mkGenitive newest ;
          AAdv            => mkLu "newly" "adv"
        }
     } ;
  lin stupid_A =
      let stupid : Str = mkLu "stupid" "adj" ;
          more   : Str = mkLu "more" "adv" ;
      in {
        s = table {
          AAdj Posit Nom  => stupid ;
          AAdj Posit Gen  => mkGenitive stupid ;
          AAdj Compar Nom => more ++ stupid ;
          AAdj Compar Gen => more ++ mkGenitive stupid ;
          AAdj Superl Nom => more ++ stupid ;
          AAdj Superl Gen => more ++ mkGenitive stupid ;
          AAdv            => mkLu "stupidly" "adv"
        }
     } ;
--     anAdjective =
--       let good   : Str = mkLu "good" "adj" "sint" ;
--           better : Str = mkLu "good" "adj" "sint" "comp" ;
--           best   : Str = mkLu "good" "adj" "sint" "sup"
--       in {
--         s = table {
--           AAdj Posit Nom  => good ;
--           AAdj Posit Gen  => mkGenitive good ;
--           AAdj Compar Nom => better ;
--           AAdj Compar Gen => mkGenitive better ;
--           AAdj Superl Nom => best ;
--           AAdj Superl Gen => mkGenitive best ;
--           AAdv            => mkLu "well" "adv"
--         }
--       } ;
-- 
--     -- Overide stuff in NounEng
--     PossNP cn np
--       = {s = \\n,c => cn.s ! n ! c ++ of_prep ++ np.s ! NPNomPoss ; g = cn.g} ;
-- 
--     PartNP cn np
--       = {s = \\n,c => cn.s ! n ! c ++ of_prep ++ np.s ! NPAcc ; g = cn.g} ;
-- 
--     CountNP det np = {
--       s = \\c => det.sp ! c ++ of_prep ++ np.s ! NPAcc ;
--       a = agrP3 det.n
--     } ;
-- 
--     RelNP np rs = {
--       s = \\c => np.s ! c ++ comma_cm ++ rs.s ! np.a ;
--       a = np.a
--     } ;
-- 

--     one_num : Str = mkLu "one" "num" "sg" ;
  -- ~~~ in_Prep ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin in_Prep = mkPrep "in" ;
  -- ~~~ and_Conj ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin and_Conj = { s1 = [] ;
                   s2 = mkLu "and" "cnjcoo" ;
                    n = Pl };
 
  oper mkPrep : Str -> Prep = \s -> lin Prep { s = mkLu s "pr" } ;

  -- ~~~ ComplVS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin ComplVS v s  = insertObj (\\_ => ExampleGeneratorEng.conjThat ++ s.s) 
                                (predV v) ;
  oper conjThat : Str = mkLu "that" "cnjsub" ;

  -- ~~~ PossNP ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin PossNP cn np = {s = \\n,c => cn.s ! n ! c ++ of_pr ++ np.s ! NPNomPoss ;
                      g = cn.g} ;

  -- ~~~ UseComp ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin UseComp comp = insertObj comp.s (predAux ExampleGeneratorEng.auxBe) ;
  oper auxBe : Aux = {
    pres = table {
      Pos => \\a => vbser.pres ! a ;
      Neg => \\a => vbser.pres ! a ++ not };
    past = table {
      Pos => \\a => vbser.past ! a ;
      Neg => \\a => vbser.past ! a ++ not };
    inf  = mkLu "be" "vbser" "inf"    ;
    ppart = mkLu "be" "vbser" "pp"    ;
    prpart = mkLu "be" "vbser" "pprs" };
  -- the verb "be"
  oper vbser : { pres : Agr => Str ; past : Agr => Str } = {
    pres = table {
      AgP1 Sg                   => mkLu "be" "vbser" "pri" "p1" "sg"  ; -- am
      AgP1 _ | AgP2 _ | AgP3Pl  => mkLu "be" "vbser" "pres"           ; -- are
      AgP3Sg _                  => mkLu "be" "vbser" "pri" "p3" "sg"  }; -- is
    past = table {
      AgP1 Sg | AgP3Sg _        => mkLu "be" "vbser" "past" "p1" "sg" ; -- was
      AgP1 _ | AgP2 _ | AgP3Pl  => mkLu "be" "vbser" "past"           } -- were
  };
  -- ~~~ for_Prep ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin for_Prep = mkPrep "for" ;
  -- ~~~ to_Prep ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin to_Prep = mkPrep "to" ;
  -- ~~~ RelNP ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin RelNP np rs = {
        s = \\c => np.s ! c ++ mkLu "," "cm" ++ rs.s ! np.a ;
        a = np.a
      } ;
  -- ~~~ Relative Pronouns ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin IdRP = { a = RNoAg ; s = table {
          RC _ (NCase Gen)
        | RC _ NPNomPoss    => mkLu "whose" "rel" "aa" "mf" "sp" ;
          RC Neutr _        => mkLu "which" "rel" "an" "mf" "sp" ;
          RC _ NPAcc        => mkLu "whom" "rel"  "an" "mf" "sp" ;
          RC _ (NCase Nom)  => mkLu "who" "rel"   "an" "mf" "sp" ;
          RPrep Neutr       => mkLu "which" "rel" "an" "mf" "sp" ;
          RPrep _           => mkLu "whom" "rel"  "an" "mf" "sp" }
  } ;
}
