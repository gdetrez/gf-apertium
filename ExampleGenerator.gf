abstract ExampleGenerator =
  Cat,
  Conjunction [ListNP],
  Grammar [ UseN, DetQuant, DetCN, NumSg, PPos, ASimul
  , PrepNP, TTAnt, IndefArt, UseCl, PredVP
  , TPres, DefArt, NumPl, AdjCN, PositA, PhrUtt
  , NoVoc, UttS, NoPConj, SlashV2a, {- MkSymb, -} SymbPN
  , UsePN, CompSlash, AdvNP, MassNP, CompoundCN, AdvVP
  , TPast, UsePron , in_Prep , and_Conj , ComplVS , PossNP
  , {- IIDig, (no digits) -} UseComp , UseV , it_Pron , PossPron
  , for_Prep , ComplVV , ConjNP , BaseNP , say_VS , to_Prep
  , AdvVPSlash , of_Prep , RelNP, AdVVP
  , he_Pron, DetNP, UseRCl, {-IDig, NumDigits,-} on_Prep
  , they_Pron, {-by_Prep, at_Prep,-} RelVP, CompNP
  , from_Prep, {-PassVPSlash,-} with_Prep {-PositAdVAdj-}
  , CompAP, IdRP, AdvS, PositAdvAdj
  , GerundN, {-pot2as3, pot1as2, num, pot0as1,-} PastPartAP
  , ProgrVP, share_N, market_N, {-NumNumeral,-} or_Conj
  , this_Quant, that_RP, PNeg, {-pot0,-} stock_N, can_VV
  , we_Pron, {-D_8,-} have_V2n
  ,  above_Prep , after_Prep , before_Prep , behind_Prep
  , between_Prep , by8agent_Prep , by8means_Prep , during_Prep
  , except_Prep , in8front_Prep , part_Prep , possess_Prep
  , through_Prep , under_Prep , without_Prep
  -- Utterances
  , UttS       -- John walks
  , UttQS      -- is it good
  , UttImpSg   -- (don't) love yourself
  , UttImpPl   -- (don't) love yourselves
  , UttImpPol  -- (don't) sleep (polite)
  , UttIP      -- who
  , UttIAdv    -- why
  , UttNP      -- this man
  , UttAdv     -- here
  , UttVP      -- to sleep
  , UttCN      -- house
  , UttCard    -- five
  , UttAP      -- fine
  , UttInterj  -- alas
    -- Cats
    , A , AP , AdV , Adv , Ant
    , CN , Card , Cl , Comp , Conj
    , Det
    , IAdv , IP , Imp
    , ListNP
    , N , NP , Num
    , PConj , PN , Phr , Pol , Prep , Pron
    , QS , Quant
    , RCl , RClTense , RP , RS
    , S
    , Temp , Tense
    , Utt
    , V , VP , VV , VV , Voc


  ] ** {
  -- We create a new category to be the top category of all examples
  cat Example ;

  -- These are the function that can create examples.
  -- mostly inspired by the ones for Utt
  fun ExampleNP : NP -> Example ;

  -- Lexicon
  --- Nouns
  fun aFeminineNoun : N ;
  fun aMasculineNoun : N ;
  --- Adjectves         Synthetic(eng)  | Pre-noun(spa)
  fun short_A  : A ; --  Yes            | No
  fun new_A    : A ; --  Yes            | Yes
  fun stupid_A : A ; --  No             | No
}
