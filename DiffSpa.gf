--# -path=.:../romance:../abstract:../common:prelude

instance DiffSpa of DiffRomance =
  open CommonRomance, PhonoSpa, BeschSpa, Prelude, ApertiumStreamFormat in {

  flags optimize=noexpand ;

  param
    Prepos = P_de | P_a ;
    VType = VHabere | VRefl ;

  oper
    dative   : Case = CPrep P_a ;
    genitive : Case = CPrep P_de ;

  -- @modified
  oper prepCase : Case -> Str = \c -> case c of {
    Nom => [] ;
    Acc => [] ; 
    CPrep P_de => mkLu "de" "pr" ; -- de
    CPrep P_a  => mkLu "a"  "pr"   -- a
  } ;

  oper artDef : Gender -> Number -> Case -> Str = \g,n,c ->
      case <g,n,c> of {
        <Masc,Sg, CPrep P_de> => "del" ;
        <Masc,Sg, CPrep P_a>  => "al" ;
        <Masc,Sg, _>          => prepCase c ++ "el" ;
        <Fem ,Sg, _> => prepCase c ++ "la" ;
        <Masc,Pl, _> => prepCase c ++ "los" ;
        <Fem ,Pl, _> => prepCase c ++ "las"
        } ;

-- In these two, "de de/du/des" becomes "de".

    artIndef = \g,n,c -> case n of {
      Sg  => prepCase c ++ genForms "un"   "una" ! g ;
      _   => prepCase c -- ++ genForms "unos" "unas" ! g   --- take this as a determiner
      } ;

    possCase = \_,_,c -> prepCase c ;

    partitive = \_,c -> prepCase c ;

{-
    partitive = \g,c -> case c of {
      CPrep P_de => "de" ;
      _ => prepCase c ++ artDef g Sg (CPrep P_de)
      } ;
-}

    conjunctCase : Case -> Case = \c -> case c of {
      Nom => Nom ;
      _ => Acc 
      } ;

    oper auxVerb : VType -> (VF => Str) = \_ -> table {
      VInfin _                      => mkLu "haber" "vbhaver" "inf" ; -- haber
      VFin (VPres Indic) Sg P1      => mkLu "haber" "vbhaver" "pri" "p1" "sg" ; -- he
      VFin (VPres Indic) Sg P2      => mkLu "haber" "vbhaver" "pri" "p2" "sg" ; -- has
      VFin (VPres Indic) Sg P3      => mkLu "haber" "vbhaver" "pri" "p3" "sg" ; -- ha
      VFin (VPres Indic) Pl P1      => mkLu "haber" "vbhaver" "pri" "p1" "pl" ; -- hemos
      VFin (VPres Indic) Pl P2      => mkLu "haber" "vbhaver" "pri" "p2" "pl" ; -- hab�is
      VFin (VPres Indic) Pl P3      => mkLu "haber" "vbhaver" "pri" "p3" "pl" ; -- han
      VFin (VPres Conjunct) Sg P1   => mkLu "haber" "vbhaver" "prs" "p1" "sg" ; -- haya
      VFin (VPres Conjunct) Sg P2   => mkLu "haber" "vbhaver" "prs" "p2" "sg" ; -- hayas
      VFin (VPres Conjunct) Sg P3   => mkLu "haber" "vbhaver" "prs" "p3" "sg" ; -- haya
      VFin (VPres Conjunct) Pl P1   => mkLu "haber" "vbhaver" "prs" "p1" "pl" ; -- hayamos
      VFin (VPres Conjunct) Pl P2   => mkLu "haber" "vbhaver" "prs" "p2" "pl" ; -- hay�is
      VFin (VPres Conjunct) Pl P3   => mkLu "haber" "vbhaver" "prs" "p3" "pl" ; -- hayan
      VFin (VImperf Indic) Sg P1    => mkLu "haber" "vbhaver" "pii" "p1" "sg" ; -- hab�a
      VFin (VImperf Indic) Sg P2    => mkLu "haber" "vbhaver" "pii" "p2" "sg" ; -- hab�as
      VFin (VImperf Indic) Sg P3    => mkLu "haber" "vbhaver" "pii" "p3" "sg" ; -- hab�a
      VFin (VImperf Indic) Pl P1    => mkLu "haber" "vbhaver" "pii" "p1" "pl" ; -- hab�amos
      VFin (VImperf Indic) Pl P2    => mkLu "haber" "vbhaver" "pii" "p2" "pl" ; -- hab�ais
      VFin (VImperf Indic) Pl P3    => mkLu "haber" "vbhaver" "pii" "p3" "pl" ; -- hab�an
      VFin (VImperf Conjunct) Sg P1 => mkLu "haber" "vbhaver" "pis" "p1" "sg" ; -- hubiera
      VFin (VImperf Conjunct) Sg P2 => mkLu "haber" "vbhaver" "pis" "p2" "sg" ; -- hubieras
      VFin (VImperf Conjunct) Sg P3 => mkLu "haber" "vbhaver" "pis" "p3" "sg" ; -- hubiera
      VFin (VImperf Conjunct) Pl P1 => mkLu "haber" "vbhaver" "pis" "p1" "pl" ; -- hubi�ramos
      VFin (VImperf Conjunct) Pl P2 => mkLu "haber" "vbhaver" "pis" "p2" "pl" ; -- hubierais
      VFin (VImperf Conjunct) Pl P3 => mkLu "haber" "vbhaver" "pis" "p3" "pl" ; -- hubieran
      VFin VPasse Sg P1             => mkLu "haber" "vbhaver" "ifi" "p1" "sg" ; -- hube
      VFin VPasse Sg P2             => mkLu "haber" "vbhaver" "ifi" "p2" "sg" ; -- hubiste
      VFin VPasse Sg P3             => mkLu "haber" "vbhaver" "ifi" "p3" "sg" ; -- hubo
      VFin VPasse Pl P1             => mkLu "haber" "vbhaver" "ifi" "p1" "pl" ; -- hubimos
      VFin VPasse Pl P2             => mkLu "haber" "vbhaver" "ifi" "p2" "pl" ; -- hubisteis
      VFin VPasse Pl P3             => mkLu "haber" "vbhaver" "ifi" "p3" "pl" ; -- hubieron
      VFin VFut Sg P1               => mkLu "haber" "vbhaver" "fti" "p1" "sg" ; -- habr�
      VFin VFut Sg P2               => mkLu "haber" "vbhaver" "fti" "p2" "sg" ; -- habr�s
      VFin VFut Sg P3               => mkLu "haber" "vbhaver" "fti" "p3" "sg" ; -- habr�
      VFin VFut Pl P1               => mkLu "haber" "vbhaver" "fti" "p1" "pl" ; -- habremos
      VFin VFut Pl P2               => mkLu "haber" "vbhaver" "fti" "p2" "pl" ; -- habr�is
      VFin VFut Pl P3               => mkLu "haber" "vbhaver" "fti" "p3" "pl" ; -- habr�n
      VFin VCondit Sg P1            => mkLu "haber" "vbhaver" "cni" "p1" "sg" ; -- habr�a
      VFin VCondit Sg P2            => mkLu "haber" "vbhaver" "cni" "p2" "sg" ; -- habr�as
      VFin VCondit Sg P3            => mkLu "haber" "vbhaver" "cni" "p3" "sg" ; -- habr�a
      VFin VCondit Pl P1            => mkLu "haber" "vbhaver" "cni" "p1" "pl" ; -- habr�amos
      VFin VCondit Pl P2            => mkLu "haber" "vbhaver" "cni" "p2" "pl" ; -- habr�ais
      VFin VCondit Pl P3            => mkLu "haber" "vbhaver" "cni" "p3" "pl" ; -- habr�an
      VImper SgP2                   => variants {} ;
      VImper PlP1                   => variants {} ;
      VImper PlP2                   => variants {} ;
      VPart Masc Sg                 => mkLu "haber" "vbhaver" "pp" "m" "sg" ; -- habido
      VPart Masc Pl                 => mkLu "haber" "vbhaver" "pp" "m" "pl" ; -- * habidos
      VPart Fem Sg                  => mkLu "haber" "vbhaver" "pp" "f" "sg" ; -- * habida
      VPart Fem Pl                  => mkLu "haber" "vbhaver" "pp" "m" "pl" ; -- * habidas
      VGer                          => mkLu "haber" "vbhaver" "ger" -- habiendo
    } ;



    partAgr : VType -> VPAgr = \vtyp -> vpAgrNone ;

    vpAgrClit : Agr -> VPAgr = \a ->
      vpAgrNone ;

    pronArg = \n,p,acc,dat -> 
      let 
        paccp = case acc of {
          CRefl   => <reflPron n p Acc, p,True> ;
          CPron ag an ap => <argPron ag an ap Acc, ap,True> ;
          _ => <[],P2,False>
          } ;
        pdatp = case dat of {
          CPron ag an ap => <argPron ag an ap dative, ap,True> ;
          _ => <[],P2,False>
          } ;
        peither = case acc of {
          CRefl | CPron _ _ _ => True ;
          _ => case dat of {
            CPron _ _ _ => True ;
            _ => False
            }
          } ;
        defaultPronArg = <pdatp.p1 ++ paccp.p1, [], peither>
----        defaultPronArg = <pdatp.p1 ++ paccp.p1, [], orB paccp.p3 pdatp.p3>
      in 
      ----  case <<paccp.p2, pdatp.p2> : Person * Person> of {
      ----     <P3,P3> => <"se" ++ paccp.p1, [], True> ;
      ----     _ => defaultPronArg
      ---     } ;
      ---- 8/6/2008 efficiency problem in pgf generation: replace the case expr with
      ---- a constant produces an error in V3 predication with two pronouns
         defaultPronArg ;

    infForm _ _ _ _  = True ;

    mkImperative b p vp =
      \\pol,g,n => 
        let 
          pe    = case b of {True => P3 ; _ => p} ;
          agr   = {g = g ; n = n ; p = pe} ;
          refl  = case vp.s.vtyp of {
            VRefl => <reflPron n pe Acc,True> ;
            _ => <[],False> 
            } ;

          clpr  =  <vp.clit1 ++ vp.clit2, [],vp.clit3.hasClit> ; 
----          clpr  = <[],[],False> ; ----e pronArg agr.n agr.p vp.clAcc vp.clDat ;
----e          verb  = case <aag.n, pol,pe> of {
----e            <Sg,Neg,P2> => (vp.s ! VPInfinit Simul clpr.p3).inf ! aag ;
----e            _ => (vp.s ! VPImperat).fin ! agr
----e            } ;
          verb  = vp.s.s ! vImper n pe ;
          neg   = vp.neg ! pol ;
          compl = neg.p2 ++ clpr.p2 ++ vp.comp ! agr ++ vp.ext ! pol
        in
        neg.p1 ++ verb ++ bindIf refl.p2 ++ refl.p1 ++ bindIf clpr.p3 ++ clpr.p1 ++ compl ;

    negation : RPolarity => (Str * Str) = table {
      RPos => <[],[]> ;
      RNeg _ => <mkLu "no" "adv",[]>
      } ;

    conjThan = "que" ;
    conjThat = "que" ;
    subjIf = "si" ;


    clitInf b cli inf = inf ++ bindIf b ++ cli ;

  -- Apertium
  oper relPron : Bool => AAgr => Case => Str = \\b,a,c =>
    case c of {
      Nom | Acc => mkLu "que" "rel" "an" "mf" "sp" ;
      CPrep P_a => mkLu "cuyo" "rel" "aa" "m" "sg" ;
      _         => prepCase c ++ mkLu "cuyo" "rel" "aa" "m" "sg"
    } ;

    pronSuch : AAgr => Str = aagrForms "t�l" "t�l" "tales" "tales" ;

    quelPron : AAgr => Str = aagrForms "cu�l" "cu�l" "cuales" "cuales" ;

    partQIndir = [] ; ---- ?

    reflPron : Number -> Person -> Case -> Str = \n,p,c -> 
        let pro = argPron Fem n p c 
        in
        case p of { 
        P3 => case c of {
          Acc | CPrep P_a => mkLu "se" "prn" "pro" "ref" "p3" "mf" "sp" ;
          _               => mkLu "s�" "prn" "tn"  "ref" "p3" "mf" "sp"
          } ;
        _ => pro
        } ; 

    argPron : Gender -> Number -> Person -> Case -> Str = 
      let 
        cases : (x,y : Str) -> Case -> Str = \me,moi,c -> case c of {
          Acc | CPrep P_a => me ;
          _ => moi
          } ;
        cases3 : (x,y,z : Str) -> Case -> Str = \les,leur,eux,c -> case c of {
          Acc => les ;
          CPrep P_a => leur ;
          _ => eux
          } ;
      in 
      \g,n,p -> case <<g,n,p> : Gender * Number * Person> of { 
        <_,Sg,P1>   => cases  (mkLu "prpers" "prn" "pro" "p1" "mf" "sg") -- me
                              (mkLu "m�"     "prn" "tn"  "p1" "mf" "sg") ; -- m�
        <_,Sg,P2>   => cases  (mkLu "prpers" "prn" "pro" "p2" "mf" "sg") -- te
                              (mkLu "ti"     "prn" "tn"  "p2" "mf" "sg") ; -- t�
        <_,Pl,P1>   => cases  (mkLu "prpers" "prn" "pro" "p1" "mf" "pl") -- nos
                              (mkLu "prpers" "prn" "tn"  "p1" "f"  "pl") ; -- nosotras --- nosotros
        <_,Pl,P2>   => cases  (mkLu "prpers" "prn" "pro" "p2" "mf" "pl") -- "vos"
                              (mkLu "prpers" "prn" "tn"  "p2" "f"  "pl") ; -- "vosotras" ; --- vosotros

        <Fem,Sg,P3> => cases3 (mkLu "prpers" "prn" "pro" "p3" "f"  "sg") -- la
                              (mkLu "prpers" "prn" "pro" "p3" "mf" "sg") -- le
                              (mkLu "prpers" "prn" "tn"  "p3" "f"  "sg") ; -- ella ;

        <_,  Sg,P3> => cases3 (mkLu "prpers" "prn" "pro" "p3" "m"  "sg") -- lo
                              (mkLu "prpers" "prn" "pro" "p3" "mf" "sg") -- le
                              (mkLu "prpers" "prn" "tn"  "p3" "m"  "sg") ; -- �l ;

        <Fem,Pl,P3> => cases3 (mkLu "prpers" "prn" "pro" "p3" "f"  "pl") -- las
                              (mkLu "prpers" "prn" "pro" "p3" "mf" "pl") -- les
                              (mkLu "prpers" "prn" "tn"  "p3" "f"  "pl") ; -- ellas ;

        <_,  Pl,P3> => cases3 (mkLu "prpers" "prn" "pro" "p3" "m"  "pl") -- los
                              (mkLu "prpers" "prn" "pro" "p3" "mf" "pl") -- les
                              (mkLu "prpers" "prn" "tn"  "p3" "m"  "pl") -- ellos
        } ;

    vRefl _ = VRefl ;
    isVRefl : VType -> Bool = \ty -> case ty of {
      VRefl => True ;
      _ => False
      } ;

    oper estar : Verbum = { s = table {
      VI Infn               => mkLu "estar" "vblex" "inf" ; -- estar
      VI Ger                => mkLu "estar" "vblex" "ger" ; -- estando
      VI Part               => mkLu "estar" "vblex" "pp" "m" "sg" ; -- estado
      VPB (Pres Ind Sg P1)  => mkLu "estar" "vblex" "pri" "p1" "sg" ; -- estoy
      VPB (Pres Ind Sg P2)  => mkLu "estar" "vblex" "pri" "p2" "sg" ; -- est�s
      VPB (Pres Ind Sg P3)  => mkLu "estar" "vblex" "pri" "p3" "sg" ; -- est�
      VPB (Pres Ind Pl P1)  => mkLu "estar" "vblex" "pri" "p1" "pl" ; -- estamos
      VPB (Pres Ind Pl P2)  => mkLu "estar" "vblex" "pri" "p2" "pl" ; -- est�is
      VPB (Pres Ind Pl P3)  => mkLu "estar" "vblex" "pri" "p3" "pl" ; -- est�n
      VPB (Pres Sub Sg P1)  => mkLu "estar" "vblex" "prs" "p1" "sg" ; -- est�
      VPB (Pres Sub Sg P2)  => mkLu "estar" "vblex" "prs" "p2" "sg" ; -- est�s
      VPB (Pres Sub Sg P3)  => mkLu "estar" "vblex" "prs" "p3" "sg" ; -- est�
      VPB (Pres Sub Pl P1)  => mkLu "estar" "vblex" "prs" "p1" "pl" ; -- estemos
      VPB (Pres Sub Pl P2)  => mkLu "estar" "vblex" "prs" "p3" "pl" ; -- est�is
      VPB (Pres Sub Pl P3)  => mkLu "estar" "vblex" "prs" "p2" "pl" ; -- est�n
      VPB (Impf Ind Sg P1)  => mkLu "estar" "vblex" "pii" "p1" "sg" ; -- estaba
      VPB (Impf Ind Sg P2)  => mkLu "estar" "vblex" "pii" "p2" "sg" ; -- estabas
      VPB (Impf Ind Sg P3)  => mkLu "estar" "vblex" "pii" "p3" "sg" ; -- estaba
      VPB (Impf Ind Pl P1)  => mkLu "estar" "vblex" "pii" "p1" "pl" ; -- est�bamos
      VPB (Impf Ind Pl P2)  => mkLu "estar" "vblex" "pii" "p2" "pl" ; -- estabais
      VPB (Impf Ind Pl P3)  => mkLu "estar" "vblex" "pii" "p3" "pl" ; -- estaban
      VPB (Impf Sub Sg P1)  => mkLu "estar" "vblex" "pis" "p1" "sg" ; -- estuviera
      VPB (Impf Sub Sg P2)  => mkLu "estar" "vblex" "pis" "p2" "sg" ; -- estuvieras
      VPB (Impf Sub Sg P3)  => mkLu "estar" "vblex" "pis" "p3" "sg" ; -- estuviera
      VPB (Impf Sub Pl P1)  => mkLu "estar" "vblex" "pis" "p1" "pl" ; -- estuvi�ramos
      VPB (Impf Sub Pl P2)  => mkLu "estar" "vblex" "pis" "p2" "pl" ; -- estuvierais
      VPB (Impf Sub Pl P3)  => mkLu "estar" "vblex" "pis" "p3" "pl" ; -- estuvieran
      VPB (ImpfSub2 Sg P1)  => mkLu "estar" "vblex" "pis" "p1" "sg" ; -- estuviese
      VPB (ImpfSub2 Sg P2)  => mkLu "estar" "vblex" "pis" "p2" "sg" ; -- estuvieses
      VPB (ImpfSub2 Sg P3)  => mkLu "estar" "vblex" "pis" "p3" "sg" ; -- estuviese
      VPB (ImpfSub2 Pl P1)  => mkLu "estar" "vblex" "pis" "p1" "pl" ; -- estuvi�semos
      VPB (ImpfSub2 Pl P2)  => mkLu "estar" "vblex" "pis" "p2" "pl" ; -- estuvieseis
      VPB (ImpfSub2 Pl P3)  => mkLu "estar" "vblex" "pis" "p3" "pl" ; -- estuviesen
      VPB (Pret Sg P1)      => mkLu "estar" "vblex" "ifi" "p1" "sg" ; -- estuve
      VPB (Pret Sg P2)      => mkLu "estar" "vblex" "ifi" "p2" "sg" ; -- estuviste
      VPB (Pret Sg P3)      => mkLu "estar" "vblex" "ifi" "p3" "sg" ; -- estuvo
      VPB (Pret Pl P1)      => mkLu "estar" "vblex" "ifi" "p1" "pl" ; -- estuvimos
      VPB (Pret Pl P2)      => mkLu "estar" "vblex" "ifi" "p2" "pl" ; -- estuvisteis
      VPB (Pret Pl P3)      => mkLu "estar" "vblex" "ifi" "p3" "pl" ; -- estuvieron
      VPB (Fut Ind Sg P1)   => mkLu "estar" "vblex" "fti" "p1" "sg" ; -- estar�
      VPB (Fut Ind Sg P2)   => mkLu "estar" "vblex" "fti" "p2" "sg" ; -- estar�s
      VPB (Fut Ind Sg P3)   => mkLu "estar" "vblex" "fti" "p3" "sg" ; -- estar�
      VPB (Fut Ind Pl P1)   => mkLu "estar" "vblex" "fti" "p1" "pl" ; -- estaremos
      VPB (Fut Ind Pl P2)   => mkLu "estar" "vblex" "fti" "p2" "pl" ; -- estar�is
      VPB (Fut Ind Pl P3)   => mkLu "estar" "vblex" "fti" "p3" "pl" ; -- estar�n
      VPB (Fut Sub Sg P1)   => mkLu "estar" "vblex" "fts" "p1" "sg" ; -- estuviere
      VPB (Fut Sub Sg P2)   => mkLu "estar" "vblex" "fts" "p2" "sg" ; -- estuvieres
      VPB (Fut Sub Sg P3)   => mkLu "estar" "vblex" "fts" "p3" "sg" ; -- estuviere
      VPB (Fut Sub Pl P1)   => mkLu "estar" "vblex" "fts" "p1" "pl" ; -- estuvi�remos
      VPB (Fut Sub Pl P2)   => mkLu "estar" "vblex" "fts" "p2" "pl" ; -- estuviereis
      VPB (Fut Sub Pl P3)   => mkLu "estar" "vblex" "fts" "p3" "pl" ; -- estuvieren
      VPB (Cond Sg P1)      => mkLu "estar" "vblex" "cni" "p1" "sg" ; -- estar�a
      VPB (Cond Sg P2)      => mkLu "estar" "vblex" "cni" "p2" "sg" ; -- estar�as
      VPB (Cond Sg P3)      => mkLu "estar" "vblex" "cni" "p3" "sg" ; -- estar�a
      VPB (Cond Pl P1)      => mkLu "estar" "vblex" "cni" "p1" "pl" ; -- estar�amos
      VPB (Cond Pl P2)      => mkLu "estar" "vblex" "cni" "p2" "pl" ; -- estar�ais
      VPB (Cond Pl P3)      => mkLu "estar" "vblex" "cni" "p3" "pl" ; -- estar�an
      VPB (Imper Sg P1)     => mkLu "estar" "vblex" "imp" "p1" "sg" ;
      VPB (Imper Sg P2)     => mkLu "estar" "vblex" "imp" "p2" "sg" ;  -- est�
      VPB (Imper Sg P3)     => mkLu "estar" "vblex" "imp" "p3" "sg" ;  -- est�
      VPB (Imper Pl P1)     => mkLu "estar" "vblex" "imp" "p1" "pl" ;  -- estemos
      VPB (Imper Pl P2)     => mkLu "estar" "vblex" "imp" "p2" "pl" ;  -- estad
      VPB (Imper Pl P3)     => mkLu "estar" "vblex" "imp" "p3" "pl" ;  -- est�n
      VPB (Pass Sg Masc)    => mkLu "estar" "vblex" "pp" "m" "sg" ; -- estado
      VPB (Pass Sg Fem)     => mkLu "estar" "vblex" "pp" "f" "sg" ; -- estada
      VPB (Pass Pl Masc)    => mkLu "estar" "vblex" "pp" "m" "pl" ; -- estados
      VPB (Pass Pl Fem)     => mkLu "estar" "vblex" "pp" "f" "pl"   -- estadas
    }};
    auxPassive : Verb = verbBeschH estar ;




    copula = { s = table {
      VInfin True                   => mkLu "ser" "vbser" "inf" ; -- ser
      VInfin False                  => mkLu "ser" "vbser" "inf" ; -- ser
      VFin (VPres Indic) Sg P1      => mkLu "ser" "vbser" "pri" "p1" "sg" ; -- soy
      VFin (VPres Indic) Sg P2      => mkLu "ser" "vbser" "pri" "p2" "sg" ; -- eres
      VFin (VPres Indic) Sg P3      => mkLu "ser" "vbser" "pri" "p3" "sg" ; -- es
      VFin (VPres Indic) Pl P1      => mkLu "ser" "vbser" "pri" "p1" "pl" ; -- som
      VFin (VPres Indic) Pl P2      => mkLu "ser" "vbser" "pri" "p2" "pl" ; -- sois
      VFin (VPres Indic) Pl P3      => mkLu "ser" "vbser" "pri" "p3" "pl" ; -- son
      VFin (VPres Conjunct) Sg P1   => mkLu "ser" "vbser" "prs" "p1" "sg" ; -- sea
      VFin (VPres Conjunct) Sg P2   => mkLu "ser" "vbser" "prs" "p2" "sg" ; -- seas
      VFin (VPres Conjunct) Sg P3   => mkLu "ser" "vbser" "prs" "p3" "sg" ; -- sea
      VFin (VPres Conjunct) Pl P1   => mkLu "ser" "vbser" "prs" "p1" "pl" ; -- seamos
      VFin (VPres Conjunct) Pl P2   => mkLu "ser" "vbser" "prs" "p2" "pl" ; -- se�is
      VFin (VPres Conjunct) Pl P3   => mkLu "ser" "vbser" "prs" "p3" "pl" ; -- sean
      VFin (VImperf Indic) Sg P1    => mkLu "ser" "vbser" "pii" "p1" "sg" ; -- era
      VFin (VImperf Indic) Sg P2    => mkLu "ser" "vbser" "pii" "p2" "sg" ; -- eras
      VFin (VImperf Indic) Sg P3    => mkLu "ser" "vbser" "pii" "p3" "sg" ; -- era
      VFin (VImperf Indic) Pl P1    => mkLu "ser" "vbser" "pii" "p1" "pl" ; -- �ramos
      VFin (VImperf Indic) Pl P2    => mkLu "ser" "vbser" "pii" "p2" "pl" ; -- erais
      VFin (VImperf Indic) Pl P3    => mkLu "ser" "vbser" "pii" "p3" "pl" ; -- eran
      VFin (VImperf Conjunct) Sg P1 => mkLu "ser" "vbser" "pis" "p1" "sg" ; -- fuera
      VFin (VImperf Conjunct) Sg P2 => mkLu "ser" "vbser" "pis" "p2" "sg" ; -- fueras
      VFin (VImperf Conjunct) Sg P3 => mkLu "ser" "vbser" "pis" "p3" "sg" ; -- fuera
      VFin (VImperf Conjunct) Pl P1 => mkLu "ser" "vbser" "pis" "p1" "pl" ; -- fu�ramos
      VFin (VImperf Conjunct) Pl P2 => mkLu "ser" "vbser" "pis" "p2" "pl" ; -- fuerais
      VFin (VImperf Conjunct) Pl P3 => mkLu "ser" "vbser" "pis" "p3" "pl" ; -- fueran
      VFin VPasse             Sg P1 => mkLu "ser" "vbser" "ifi" "p1" "sg" ; -- fui
      VFin VPasse             Sg P2 => mkLu "ser" "vbser" "ifi" "p2" "sg" ; -- fuiste
      VFin VPasse             Sg P3 => mkLu "ser" "vbser" "ifi" "p3" "sg" ; -- fue
      VFin VPasse             Pl P1 => mkLu "ser" "vbser" "ifi" "p1" "pl" ; -- fuimos
      VFin VPasse             Pl P2 => mkLu "ser" "vbser" "ifi" "p2" "pl" ; -- fuisteis
      VFin VPasse             Pl P3 => mkLu "ser" "vbser" "ifi" "p3" "pl" ; -- fueron
      VFin VFut               Sg P1 => mkLu "ser" "vbser" "fti" "p1" "sg" ; -- ser�
      VFin VFut               Sg P2 => mkLu "ser" "vbser" "fti" "p2" "sg" ; -- ser�s
      VFin VFut               Sg P3 => mkLu "ser" "vbser" "fti" "p3" "sg" ; -- ser�
      VFin VFut               Pl P1 => mkLu "ser" "vbser" "fti" "p1" "pl" ; -- seremos
      VFin VFut               Pl P2 => mkLu "ser" "vbser" "fti" "p2" "pl" ; -- ser�is
      VFin VFut               Pl P3 => mkLu "ser" "vbser" "fti" "p3" "pl" ; -- ser�n
      VFin VCondit            Sg P1 => mkLu "ser" "vbser" "cni" "p1" "sg" ; -- ser�a
      VFin VCondit            Sg P2 => mkLu "ser" "vbser" "cni" "p2" "sg" ; -- ser�as
      VFin VCondit            Sg P3 => mkLu "ser" "vbser" "cni" "p3" "sg" ; -- ser�a
      VFin VCondit            Pl P1 => mkLu "ser" "vbser" "cni" "p1" "pl" ; -- ser�amos
      VFin VCondit            Pl P2 => mkLu "ser" "vbser" "cni" "p2" "pl" ; -- ser�ais
      VFin VCondit            Pl P3 => mkLu "ser" "vbser" "cni" "p3" "pl" ; -- ser�an
      VImper                  SgP2  => mkLu "ser" "vbser" "imp" "p2" "sg" ; -- s�
      VImper                  PlP1  => mkLu "ser" "vbser" "imp" "p1" "pl" ; -- seamos
      VImper                  PlP2  => mkLu "ser" "vbser" "imp" "p2" "pl" ; -- sed
      VPart Masc              Sg    => mkLu "ser" "vbser" "pp" "m" "sg" ; -- sido
      VPart Masc              Pl    => mkLu "ser" "vbser" "pp" "m" "pl" ; -- sidos *not in apertium
      VPart Fem               Sg    => mkLu "ser" "vbser" "pp" "f" "sg" ; -- sida *not in apertium
      VPart Fem               Pl    => mkLu "ser" "vbser" "pp" "f" "pl" ; -- sidas                       
      VGer                          => mkLu "ser" "vbser" "ger" -- siendo                             
     }; vtyp = VHabere } ;

    haber_V : Verb = verbBeschH (haber_3 "haber") ;

    verbBeschH : Verbum -> Verb = \v -> verbBesch v ** {vtyp = VHabere} ;

    subjPron = \_ -> [] ;

    polNegDirSubj = RPos ;

}
