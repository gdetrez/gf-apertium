concrete ExampleGeneratorSpa of ExampleGenerator =
  CatSpa,
  GrammarSpa [
    -- Functions
    ASimul, AdVVP, AdjCN, AdvNP, AdvS, AdvVP, AdvVPSlash
  , BaseNP
  , CompAP, CompNP, CompSlash, ComplVV, {-ComplVS-} CompoundCN, ConjNP
  , {-DefArt,-} DetCN, DetNP, DetQuant
  , GenNP, GerundN
  , {-IIDig,-} IdRP {-IndefArt-}
  , MassNP {-MkSymb,-}
  , NoPConj, NoVoc, NumPl, NumSg
  , PNeg, PPos, PastPartAP, PhrUtt, PositA, PositAdvAdj, PossNP
  , PossPron, {-PredVP,-} PrepNP {-ProgrVP-}
  , RelNP, RelVP
  {-SlashV2a,-} {-SymbPN-}
  , TPast, TPres, TTAnt
  , UseCl, UseComp, UseN, UsePN, UsePron, UseQuantPN, UseRCl, UseV, UttS
  -- Words
  , can_VV, {-for_Prep,-} {-from_Prep,-} have_V2kn {-he_Pron,-} {-in_Prep,-}
  , {-it_Pron,-} market_N, of_Prep {-on_Prep,-} {-or_Conj,-} {-pot0,-}
  , say_VS, share_N, stock_N, that_RP {-they_Pron-} {-this_Quant-} {-to_Prep-} {-we_Pron-}
  -- Utterances
  , UttS       -- John walks
  , UttQS      -- is it good
  , UttImpSg   -- (don't) love yourself
  , UttImpPl   -- (don't) love yourselves
  , UttImpPol  -- (don't) sleep (polite)
  , UttIP      -- who
  , UttIAdv    -- why
  , UttNP      -- this man
  , UttAdv     -- here
  , UttVP      -- to sleep
  , UttCN      -- house
  , UttCard    -- five
  , UttAP      -- fine
  , UttInterj  -- alas
  -- Cats
  , A , AP , AdV , Adv , Ant
  , CN , Card , Cl , Comp , Conj
  , Det
  , IAdv , IP , Imp
  , ListNP
  , N , NP , Num
  , PConj , PN , Phr , Pol , Prep , Pron
  , QS , Quant
  , RCl , RClTense , RP , RS
  , S
  , Temp , Tense
  , Utt
  , V , VP , VV , VV , Voc
  ]
  ** open ResSpa, BeschSpa, CommonRomance, ApertiumStreamFormat, Prelude in {

  flags coding=utf8 ;

  -- --- PredVP -----------------------------------------------------------

  oper auxVerb : VType -> (VF => Str) = \_ -> table {
    VInfin _                      => mkLu "haber" "vbhaver" "inf" ; -- haber
    VFin (VPres Indic) Sg P1      => mkLu "haber" "vbhaver" "pri" "p1" "sg" ; -- he
    VFin (VPres Indic) Sg P2      => mkLu "haber" "vbhaver" "pri" "p2" "sg" ; -- has
    VFin (VPres Indic) Sg P3      => mkLu "haber" "vbhaver" "pri" "p3" "sg" ; -- ha
    VFin (VPres Indic) Pl P1      => mkLu "haber" "vbhaver" "pri" "p1" "pl" ; -- hemos
    VFin (VPres Indic) Pl P2      => mkLu "haber" "vbhaver" "pri" "p2" "pl" ; -- habéis
    VFin (VPres Indic) Pl P3      => mkLu "haber" "vbhaver" "pri" "p3" "pl" ; -- han
    VFin (VPres Conjunct) Sg P1   => mkLu "haber" "vbhaver" "prs" "p1" "sg" ; -- haya
    VFin (VPres Conjunct) Sg P2   => mkLu "haber" "vbhaver" "prs" "p2" "sg" ; -- hayas
    VFin (VPres Conjunct) Sg P3   => mkLu "haber" "vbhaver" "prs" "p3" "sg" ; -- haya
    VFin (VPres Conjunct) Pl P1   => mkLu "haber" "vbhaver" "prs" "p1" "pl" ; -- hayamos
    VFin (VPres Conjunct) Pl P2   => mkLu "haber" "vbhaver" "prs" "p2" "pl" ; -- hayáis
    VFin (VPres Conjunct) Pl P3   => mkLu "haber" "vbhaver" "prs" "p3" "pl" ; -- hayan
    VFin (VImperf Indic) Sg P1    => mkLu "haber" "vbhaver" "pii" "p1" "sg" ; -- había
    VFin (VImperf Indic) Sg P2    => mkLu "haber" "vbhaver" "pii" "p2" "sg" ; -- habías
    VFin (VImperf Indic) Sg P3    => mkLu "haber" "vbhaver" "pii" "p3" "sg" ; -- había
    VFin (VImperf Indic) Pl P1    => mkLu "haber" "vbhaver" "pii" "p1" "pl" ; -- habíamos
    VFin (VImperf Indic) Pl P2    => mkLu "haber" "vbhaver" "pii" "p2" "pl" ; -- habíais
    VFin (VImperf Indic) Pl P3    => mkLu "haber" "vbhaver" "pii" "p3" "pl" ; -- habían
    VFin (VImperf Conjunct) Sg P1 => mkLu "haber" "vbhaver" "pis" "p1" "sg" ; -- hubiera
    VFin (VImperf Conjunct) Sg P2 => mkLu "haber" "vbhaver" "pis" "p2" "sg" ; -- hubieras
    VFin (VImperf Conjunct) Sg P3 => mkLu "haber" "vbhaver" "pis" "p3" "sg" ; -- hubiera
    VFin (VImperf Conjunct) Pl P1 => mkLu "haber" "vbhaver" "pis" "p1" "pl" ; -- hubiéramos
    VFin (VImperf Conjunct) Pl P2 => mkLu "haber" "vbhaver" "pis" "p2" "pl" ; -- hubierais
    VFin (VImperf Conjunct) Pl P3 => mkLu "haber" "vbhaver" "pis" "p3" "pl" ; -- hubieran
    VFin VPasse Sg P1             => mkLu "haber" "vbhaver" "ifi" "p1" "sg" ; -- hube
    VFin VPasse Sg P2             => mkLu "haber" "vbhaver" "ifi" "p2" "sg" ; -- hubiste
    VFin VPasse Sg P3             => mkLu "haber" "vbhaver" "ifi" "p3" "sg" ; -- hubo
    VFin VPasse Pl P1             => mkLu "haber" "vbhaver" "ifi" "p1" "pl" ; -- hubimos
    VFin VPasse Pl P2             => mkLu "haber" "vbhaver" "ifi" "p2" "pl" ; -- hubisteis
    VFin VPasse Pl P3             => mkLu "haber" "vbhaver" "ifi" "p3" "pl" ; -- hubieron
    VFin VFut Sg P1               => mkLu "haber" "vbhaver" "fti" "p1" "sg" ; -- habré
    VFin VFut Sg P2               => mkLu "haber" "vbhaver" "fti" "p2" "sg" ; -- habrás
    VFin VFut Sg P3               => mkLu "haber" "vbhaver" "fti" "p3" "sg" ; -- habrá
    VFin VFut Pl P1               => mkLu "haber" "vbhaver" "fti" "p1" "pl" ; -- habremos
    VFin VFut Pl P2               => mkLu "haber" "vbhaver" "fti" "p2" "pl" ; -- habréis
    VFin VFut Pl P3               => mkLu "haber" "vbhaver" "fti" "p3" "pl" ; -- habrán
    VFin VCondit Sg P1            => mkLu "haber" "vbhaver" "cni" "p1" "sg" ; -- habría
    VFin VCondit Sg P2            => mkLu "haber" "vbhaver" "cni" "p2" "sg" ; -- habrías
    VFin VCondit Sg P3            => mkLu "haber" "vbhaver" "cni" "p3" "sg" ; -- habría
    VFin VCondit Pl P1            => mkLu "haber" "vbhaver" "cni" "p1" "pl" ; -- habríamos
    VFin VCondit Pl P2            => mkLu "haber" "vbhaver" "cni" "p2" "pl" ; -- habríais
    VFin VCondit Pl P3            => mkLu "haber" "vbhaver" "cni" "p3" "pl" ; -- habrían
    VImper SgP2                   => variants {} ;
    VImper PlP1                   => variants {} ;
    VImper PlP2                   => variants {} ;
    VPart Masc Sg                 => mkLu "haber" "vbhaver" "pp" "m" "sg" ; -- habido
    VPart Masc Pl                 => mkLu "haber" "vbhaver" "pp" "m" "pl" ; -- * habidos
    VPart Fem Sg                  => mkLu "haber" "vbhaver" "pp" "f" "sg" ; -- * habida
    VPart Fem Pl                  => mkLu "haber" "vbhaver" "pp" "m" "pl" ; -- * habidas
    VGer                          => mkLu "haber" "vbhaver" "ger" -- habiendo
  } ;


  oper reflPron : Number -> Person -> Case -> Str = \n,p,c ->
    case <n,p,c> of {
      <Sg,P1,Nom | CPrep P_de>  => mkLu "mí" "prn" "tn" "p1" "mf" "sg" ; -- mí
      <Sg,P1,Acc | CPrep P_a>   => mkLu "prpers" "prn" "pro" "p1" "mf" "sg" ; -- me
      <Sg,P2,Nom | CPrep P_de>  => mkLu "tí" "prn" "tn" "p2" "mf" "sg" ; -- * tí
      <Sg,P2,Acc | CPrep P_a>   => mkLu "prpers" "prn" "pro" "p2" "mf" "sg" ;-- te
      <Sg,P3,Nom | CPrep P_de>  => mkLu "sí" "prn" "tn" "ref" "p3" "mf" "sp" ; -- sí
      <Sg,P3,Acc | CPrep P_a>   => mkLu "se" "prn" "pro" "ref" "p3" "mf" "sp" ; -- se
      <Pl,P1,Nom | CPrep P_de>  => mkLu "prpers" "prn" "tn" "p1" "f" "pl" ; -- nosotras
      <Pl,P1,Acc | CPrep P_a>   => mkLu "prpers" "prn" "pro" "p1" "mf" "pl" ; -- nos
      <Pl,P2,Nom | CPrep P_de>  => mkLu "prpers" "prn" "tn" "p2" "f" "pl" ; -- vosotras
      <Pl,P2,Acc | CPrep P_a>   => mkLu "prpers" "prn" "pro" "p2" "mf" "pl" ; -- * vos
      <Pl,P3,Nom | CPrep P_de>  => mkLu "sí" "prn" "tn" "ref" "p3" "mf" "sp" ; -- sí
      <Pl,P3,Acc | CPrep P_a>   => mkLu "se" "prn" "pro" "ref" "p3" "mf" "sp" -- se
    } ;

  -- isNeg = True if subject NP is a negative element, e.g. "personne"
  oper mkClausePol : Bool -> Str -> Bool -> Bool -> Agr -> CatSpa.VP ->
    {s : Direct => RTense => Anteriority => RPolarity => Mood => Str} =
    \isNeg, subj, hasClit, isPol, agr, vp -> {
      s = \\d,te,a,b,m =>
        let
          pol : RPolarity = case <isNeg, vp.isNeg, b, d> of {
            <_,True,RPos,_>    => RNeg True ; 
            <True,_,RPos,DInv> => RNeg True ; 
            <True,_,RPos,_>    => polNegDirSubj ;
            _ => b
          } ;
          neg = vp.neg ! pol ;
          gen = agr.g ;
          num = agr.n ;
          per = agr.p ;
          compl = case isPol of {
            True => vp.comp ! {g = gen ; n = Sg ; p = per} ;
            _ => vp.comp ! agr
          } ;
          ext = vp.ext ! pol ;
          vtyp  = vp.s.vtyp ;
          refl  = case isVRefl vtyp of {
            True => ExampleGeneratorSpa.reflPron num per Acc ; ---- case ?
              _ => []
          } ;
          clit  = refl ++ vp.clit1 ++ vp.clit2 ++ vp.clit3.s ; ---- refl first?
          verb = vp.s.s ;
          vaux = ExampleGeneratorSpa.auxVerb vp.s.vtyp ;
          vagr = appVPAgr vp.agr (aagr gen num) ; --- subtype bug
          part = verb ! VPart vagr.g vagr.n ;
          vps : Str * Str = case <te,a> of {
            <RPast,Simul> => <verb ! VFin (VImperf m) num per, []> ; --# notpresent
            <RPast,Anter> => <vaux ! VFin (VImperf m) num per, part> ; --# notpresent
            <RFut,Simul>  => <verb ! VFin (VFut) num per, []> ; --# notpresent
            <RFut,Anter>  => <vaux ! VFin (VFut) num per, part> ; --# notpresent
            <RCond,Simul> => <verb ! VFin (VCondit) num per, []> ; --# notpresent
            <RCond,Anter> => <vaux ! VFin (VCondit) num per, part> ; --# notpresent
            <RPasse,Simul> => <verb ! VFin (VPasse) num per, []> ; --# notpresent
            <RPasse,Anter> => <vaux ! VFin (VPasse) num per, part> ; --# notpresent
            <RPres,Anter> => <vaux ! VFin (VPres m) num per, part> ; --# notpresent
            <RPres,Simul> => <verb ! VFin (VPres m) num per, []> 
          } ;
          fin = vps.p1 ;
          inf = vps.p2 ;
          in case d of {
            DDir =>
              subj ++ neg.p1 ++ clit ++ fin ++ neg.p2 ++ inf ++ compl ++ ext ;
            DInv =>
              invertedClause vp.s.vtyp <te, a, num, per> hasClit neg clit fin inf compl subj ext
          }
      } ;

  lin PredVP np vp = ExampleGeneratorSpa.mkClausePol np.isNeg (np.s ! Nom).comp np.hasClit np.isPol np.a vp ;

  -- Examples
  lincat Example = { s : Str } ;
  lin ExampleNP np = {s = (np.s ! Nom).ton} ;

  -- --- Adverbs ------------------------------------------------------------
--   lin there_Adv = { s = mkLu "allí" "adv" } ;
--       there7from_Adv = { s = de_pr ++ ExampleGeneratorSpa.there_Adv.s } ;
--       there7to_Adv = { s = para_pr ++ ExampleGeneratorSpa.there_Adv.s } ;
--       here_Adv = { s = mkLu "aquí" "adv" } ;
--       here7from_Adv = { s = de_pr ++ ExampleGeneratorSpa.here_Adv.s } ;
--       here7to_Adv = { s = para_pr ++ ExampleGeneratorSpa.here_Adv.s } ;
--       everywhere_Adv = { s = mkLu "en todas partes" "adv" } ;
--       somewhere_Adv = { s = mkLu "en" "pr"
--                         ++ mkLu "alguno" "det" "qnt" "f" "sg"
--                         ++ mkLu "parte" "n" "f" "sg" } ;
-- 
--   -- AdA: adjective-modifying adverb e.g. "very"
--   oper mkAdA : Str -> Adv = \s -> lin Adv { s = mkLu s "preadv" } ;
--   lin
--     almost_AdA = ss (adv "casi");
--     quite_Adv = ss (mkLu "bastante" "preadv") ;
--     so_AdA = ss (adv "tanto") ;
--     too_AdA = ss (mkLu "demasiado" "preadv");
--     very_AdA = ss (mkLu "muy" "preadv");
--   -- CAdv: comparative adverb
--   lin
--     as_CAdv   = { s = mkLu "si" "cnjadv"  ; p = mkLu "que" "cnjsub" } ;
--     less_CAdv = { s = mkLu "menos" "adv"  ; p = mkLu "que" "cnjsub" } ;
--     more_CAdv = { s = mkLu "más" "preadv" ; p = mkLu "que" "cnjsub" } ;
-- 
  -- --- Conjunctions --------------------------------------------------------
  oper mkNewConj : Conj -> Str -> Str -> Conj
    = \conj,si,entonces -> lin Conj { s1 = si ; s2 = entonces ; n = conj.n } ;

--   lin either7or_DConj = mkNewConj GrammarSpa.either7or_DConj
--         (mkLu "o" "cnjcoo") (mkLu "o" "cnjcoo") ; -- o ... o
  lin or_Conj         = mkNewConj GrammarSpa.or_Conj
                          [] (mkLu "o" "cnjcoo") ; -- o
--       and_Conj        = mkNewConj GrammarSpa.and_Conj
--         [] (mkLu "y" "cnjcoo") ; -- y
--       both7and_DConj  = mkNewConj GrammarSpa.both7and_DConj
--         (mkLu "y" "cnjcoo") (mkLu "y" "cnjcoo") ; -- y ... y
--       if_then_Conj    = mkNewConj GrammarSpa.if_then_Conj
--         (mkLu "si" "cnjadv") (mkLu "entonces" "cnjadv") ; -- si ... entonces
-- 
--   -- --- Determiners --------------------------------------------------------
--   oper mkNewDet : Det -> Str -> Str -> Str -> Str -> Det
--     = \det,muchos_det,muchas_det,muchos_prn,muchas_prn -> lin Det
--     { s = table Gender [mkCases muchos_det ; mkCases muchas_det] ;
--       isNeg = det.isNeg;
--       n = det.n;
--       s2 = det.s2;
--       sp = table Gender [mkCases muchos_prn ; mkCases muchas_prn] };
-- 
--   lin every_Det = mkNewDet GrammarSpa.every_Det
--         (mkLu "cada" "det" "ind" "mf" "sg") -- cada
--         (mkLu "cada" "det" "ind" "mf" "sg") -- cada
--         (mkLu "cada" "det" "ind" "mf" "sg") -- cada
--         (mkLu "cada" "det" "ind" "mf" "sg") ; -- cada
--       few_Det    = mkNewDet GrammarSpa.few_Det
--         (mkLu "poco" "det" "qnt" "m" "pl") -- pocos
--         (mkLu "poco" "det" "qnt" "f" "pl") -- pocas
--         (mkLu "pocos" "prn" "tn" "m" "pl") -- pocos
--         (mkLu "pocos" "prn" "tn" "f" "pl") ; -- pocas
--       many_Det   = mkNewDet GrammarSpa.many_Det
--         (mkLu "mucho" "det" "qnt" "m" "pl") -- muchos
--         (mkLu "mucho" "det" "qnt" "f" "pl") -- muchas
--         (mkLu "muchos" "prn" "tn" "m" "pl") -- muchos
--         (mkLu "muchos" "prn" "tn" "f" "pl") ; -- muchas
--       much_Det   = mkNewDet GrammarSpa.much_Det
--         (mkLu "mucho" "det" "qnt" "m" "sg") -- mucho
--         (mkLu "mucho" "det" "qnt" "f" "sg") -- mucha
--         (mkLu "mucho" "det" "qnt" "m" "sg") -- mucho
--         (mkLu "mucho" "det" "qnt" "f" "sg") ; -- mucha
--       somePl_Det = mkNewDet GrammarSpa.somePl_Det
--         (mkLu "alguno" "det" "qnt" "m" "pl") -- algunos
--         (mkLu "alguno" "det" "qnt" "f" "pl") -- algunas
--         (mkLu "alguno" "prn" "tn" "m" "pl") -- algunos
--         (mkLu "alguno" "prn" "tn" "f" "pl") ; -- algunas
--       someSg_Det = mkNewDet GrammarSpa.someSg_Det
--         (mkLu "alguno" "det" "qnt" "m" "sg") -- algún
--         (mkLu "alguno" "det" "qnt" "f" "sg") -- alguna
--         (mkLu "alguno" "prn" "tn" "m" "sg") -- algún
--         (mkLu "alguno" "prn" "tn" "f" "sg") ; -- alguna
-- 
  -- --- Quantifiers --------------------------------------------------------
  oper mkNewQuant : Quant -> (_,_,_,_,_,_,_,_:Str) -> Quant
    = \quant, este,esta,estos,estas,_este,_esta,_estos,_estas -> lin Quant
    { s = \\_ => table {
        Sg => table {
          Masc => mkCases este ;
          Fem  => mkCases esta };
        Pl => table {
          Masc => mkCases estos ;
          Fem  => mkCases estas }
      } ;
      isNeg  = quant.isNeg;
      s2 = quant.s2;
      sp = table {
        Sg => table {
          Masc => mkCases _este ;
          Fem  => mkCases _esta };
        Pl => table {
          Masc => mkCases _estos ;
          Fem  => mkCases _estas }
      }
    };

--   lin no_Quant   = mkNewQuant GrammarSpa.no_Quant
--         (mkLu "ninguno" "det" "ind" "m" "sg") -- ningún
--         (mkLu "ninguno" "det" "ind" "f" "sg") -- ninguna
--         (mkLu "ninguno" "prn" "tn" "m" "pl") -- ningunos
--         (mkLu "ninguno" "prn" "tn" "f" "pl") -- ningunas
--         (mkLu "ninguno" "det" "ind" "m" "sg") -- ningún
--         (mkLu "ninguno" "prn" "tn" "f" "sg") -- ninguna
--         (mkLu "ninguno" "prn" "tn" "m" "pl") -- ningunos
--         (mkLu "ninguno" "prn" "tn" "f" "pl") ; -- ningunas
  lin this_Quant = mkNewQuant GrammarSpa.this_Quant
        (mkLu "este" "det" "dem" "m" "sg") -- este
        (mkLu "este" "det" "dem" "f" "sg") -- esta
        (mkLu "este" "det" "dem" "m" "pl") -- estos
        (mkLu "este" "det" "dem" "f" "pl") -- estas
        (mkLu "éste" "prn" "tn" "m" "sg") -- éste
        (mkLu "éste" "prn" "tn" "f" "sg") -- ésta
        (mkLu "éste" "prn" "tn" "m" "pl") -- éstos
        (mkLu "éste" "prn" "tn" "f" "pl") ; -- éstas
--       that_Quant = mkNewQuant GrammarSpa.that_Quant
--         (mkLu "ese" "det" "dem" "m" "sg") -- ese
--         (mkLu "ese" "det" "dem" "f" "sg") -- esa
--         (mkLu "ese" "det" "dem" "m" "pl") -- esos
--         (mkLu "ese" "det" "dem" "f" "pl") -- esas
--         (mkLu "ése" "prn" "tn" "m" "sg") -- ése
--         (mkLu "ése" "prn" "tn" "f" "sg") -- ésa
--         (mkLu "ése" "prn" "tn" "m" "pl") -- ésos
--         (mkLu "ése" "prn" "tn" "f" "pl") ; -- ésas
-- 
--   -- --- Noun phrase --------------------------------------------------------
--   -- lincat is in ResRomance.gf:
--   --  { s : Case => {c1,c2,comp,ton : Str} ;
--   --    a : Agr ;
--   --    hasClit : Bool ;
--   --    isPol : Bool ;      -- only needed for French complement agr
--   --    isNeg : Bool };     -- needed for negative NP's such as "personne"
-- 
--   -- This function replace the strings component of an NP like somebody_NP
--   -- but keeps all the other info untouched
--   oper mkNewNP : NP -> Str -> NP
--     = \np,s -> lin NP {
--       s = \\c => {
--           c1 = "" ; c2 = "" ;
--           comp = case c of {
--             CPrep P_de => de_pr ++ s ;
--             CPrep P_a  => a_pr ++ s ;
--             _ => s } ;
--           ton = case c of {
--             CPrep P_de => de_pr ++ s ;
--             CPrep P_a  => a_pr ++ s ;
--             _ => s }
--         };
--         a = np.a ;
--         hasClit = np.hasClit ;
--         isPol = np.isPol ;
--         isNeg = np.isNeg
--       } ;
--   lin somebody_NP =
--     mkNewNP GrammarSpa.somebody_NP (mkLu "alguien" "prn" "tn" "m" "sg") ;
--   lin everybody_NP =
--     mkNewNP GrammarSpa.everybody_NP (mkLu "todo" "prn" "tn" "m" "pl") ;
--   lin nobody_NP =
--     mkNewNP GrammarSpa.nobody_NP (mkLu "nadie" "prn" "tn" "m" "sg") ;
--   lin something_NP =
--     mkNewNP GrammarSpa.something_NP (mkLu "algo" "prn" "tn" "m" "sg") ;
--   lin everything_NP =
--     mkNewNP GrammarSpa.everything_NP (mkLu "todo" "prn" "tn" "m" "sg") ;
--   lin nothing_NP =
--    mkNewNP GrammarSpa.nothing_NP (mkLu "nada" "prn" "tn" "m" "sg") ;
-- 
--   -- --- Pre-determiners ----------------------------------------------------
  -- This function covers a pattern that appears often where
  -- given a string s, you build a table [ s | s | de s | a s ]
  -- depending on the gender
  oper mkCases : Str -> ( Case => Str )
    = \s -> table Case [ s ; s ; de_pr ++ s ; a_pr ++ s ] ;
--   mkNewPredet : Predet -> (_,_,_,_:Str) -> Predet
--     = \predet,todo,todos,toda,todas -> lin Predet {
--       s = table {g : Gender; n : Number}
--         [ mkCases todo
--         ; mkCases todos
--         ; mkCases toda
--         ; mkCases todas ] ;
--       a = predet.a;
--       c = predet.c
--     };
-- 
--   lin
--     all_Predet  = mkNewPredet GrammarSpa.all_Predet
--       (mkLu "todo" "predet" "m" "sg") -- todo
--       (mkLu "todo" "predet" "m" "pl") -- todos
--       (mkLu "todo" "predet" "f" "sg") -- toda
--       (mkLu "todo" "predet" "f" "pl") ; -- todas
--     most_Predet = mkNewPredet GrammarSpa.most_Predet
--       (mkLu "el" "det" "def" "f" "sg"
--       ++ mkLu "mayor" "adj" "mf" "sg"
--       ++ mkLu "parte" "n" "f" "sg") -- la mayor parte
--       (mkLu "el" "det" "def" "f" "sg"
--       ++ mkLu "mayor" "adj" "mf" "sg"
--       ++ mkLu "parte" "n" "f" "sg") -- la mayor parte
--       (mkLu "el" "det" "def" "f" "sg"
--       ++ mkLu "mayor" "adj" "mf" "sg"
--       ++ mkLu "parte" "n" "f" "sg") -- la mayor parte
--       (mkLu "el" "det" "def" "f" "sg"
--       ++ mkLu "mayor" "adj" "mf" "sg"
--       ++ mkLu "parte" "n" "f" "sg") ; -- la mayor parte
--     only_Predet = mkNewPredet GrammarSpa.only_Predet
--       (mkLu "solamente" "adv") -- solamente
--       (mkLu "solamente" "adv") -- solamente
--       (mkLu "solamente" "adv") -- solamente
--       (mkLu "solamente" "adv") ; -- solamente
--     not_Predet  = mkNewPredet GrammarSpa.not_Predet
--       (mkLu "no" "adv") -- no
--       (mkLu "no" "adv") -- no
--       (mkLu "no" "adv") -- no
--       (mkLu "no" "adv") ; -- no
-- 
  -- --- Prepositions -------------------------------------------------------

  -- Tags a lemma with the <pr> POS
  oper pr : Str -> Str = \s -> mkLu s "pr" ;

  -- Tags a lemma with the <adv> POS
  oper adv : Str -> Str = \s -> mkLu s "adv" ;

  oper a_pr : Str = pr "a";
  oper de_pr : Str = pr "de";
  oper para_pr : Str = pr "para";

--   oper mkNewPrep : Prep -> Str -> Prep
--     = \prep,s -> lin Prep
--     { s = s ; c = prep.c ; isDir = prep.isDir } ;
-- 
  lin above_Prep    = mkNewPrep GrammarSpa.above_Prep (pr "sobre") ;
  lin after_Prep    = mkNewPrep GrammarSpa.after_Prep (adv "después") ;
  lin before_Prep   = mkNewPrep GrammarSpa.before_Prep (adv "antes") ;
  lin behind_Prep   = mkNewPrep GrammarSpa.behind_Prep (adv "detrás") ;
  lin between_Prep  = mkNewPrep GrammarSpa.between_Prep (pr "entre") ;
  lin by8agent_Prep = mkNewPrep GrammarSpa.by8agent_Prep (pr "por") ;
  lin by8means_Prep = mkNewPrep GrammarSpa.by8means_Prep (pr "por") ;
  lin during_Prep   = mkNewPrep GrammarSpa.during_Prep (pr "durante") ;
  lin except_Prep   = mkNewPrep GrammarSpa.except_Prep (pr "excepto") ;
  lin for_Prep      = mkNewPrep GrammarSpa.for_Prep (pr "para") ;
  lin from_Prep     = mkNewPrep GrammarSpa.from_Prep "" ;
  lin in8front_Prep = mkNewPrep GrammarSpa.in8front_Prep (adv "delante") ;
  lin in_Prep       = mkNewPrep GrammarSpa.in_Prep (pr "en") ;
  lin on_Prep       = mkNewPrep GrammarSpa.on_Prep (pr "sobre") ;
  lin part_Prep     = mkNewPrep GrammarSpa.part_Prep "" ;
  lin possess_Prep  = mkNewPrep GrammarSpa.possess_Prep "" ;
  lin through_Prep  = mkNewPrep GrammarSpa.through_Prep (pr "por") ;
  lin under_Prep    = mkNewPrep GrammarSpa.under_Prep (pr "bajo") ;
  lin with_Prep     = mkNewPrep GrammarSpa.with_Prep (pr "con") ;
  lin without_Prep  = mkNewPrep GrammarSpa.without_Prep (pr "sin") ;

--   -- --- Pronouns -----------------------------------------------------------
  -- Function that modify the strings of a pronoun:
  oper mkNewPron : Pron -> (s1,s2,s3,s4,s5,s6,s7,s8 : Str) -> Pron
    = \pron,yo,mi,me1,me2,mi_m,mi_f,mis_m,mis_f -> lin Pron
    { s = table Case
        [ {c1 = []; c2 = []; comp = yo; ton = mi};
          {c1 = me1; c2 = []; comp = []; ton = mi};
          {c1 = []; c2 = []; comp = de_pr ++ mi; ton = de_pr ++ mi};
          {c1 = []; c2 = me2; comp = []; ton = a_pr ++ mi} ];
      a = pron.a ;
      hasClit = pron.hasClit ;
      isPol = pron.isPol ;
      poss = table Number
        [ table Gender [mi_m; mi_f];
          table Gender [mis_m; mis_f] ];
    } ;

  lin i_Pron = mkNewPron GrammarSpa.i_Pron
    (mkLu "prpers" "prn" "tn" "p1" "mf" "sg")
    (mkLu "mí" "prn" "tn" "p1" "mf" "sg")
    (mkLu "prpers" "prn" "pro" "p1" "mf" "sg")
    (mkLu "prpers" "prn" "pro" "p1" "mf" "sg")
    (mkLu "mío" "det" "pos" "mf" "sg")
    (mkLu "mío" "det" "pos" "mf" "sg")
    (mkLu "mío" "det" "pos" "mf" "pl")
    (mkLu "mío" "det" "pos" "mf" "pl") ;
  lin youSg_Pron = mkNewPron GrammarSpa.youSg_Pron
    (mkLu "prpers" "prn" "tn" "p2" "mf" "sg")
    (mkLu "ti" "prn" "tn" "p2" "mf" "sg")
    (mkLu "prpers" "prn" "pro" "p2" "mf" "sg")
    (mkLu "prpers" "prn" "pro" "p2" "mf" "sg")
    (mkLu "tuyo" "det" "pos" "mf" "sg")
    (mkLu "tuyo" "det" "pos" "mf" "sg")
    (mkLu "tuyo" "det" "pos" "mf" "pl")
    (mkLu "tuyo" "det" "pos" "mf" "pl") ;
  lin youPl_Pron   = mkNewPron GrammarSpa.youPl_Pron
    (mkLu "prpers" "prn" "tn" "p2" "m" "pl")
    (mkLu "prpers" "prn" "tn" "p2" "m" "pl")
    (mkLu "prpers" "prn" "pro" "p2" "mf" "pl")
    (mkLu "prpers" "prn" "pro" "p2" "mf" "pl")
    (mkLu "vuestro" "det" "pos" "m" "sg")
    (mkLu "vuestro" "det" "pos" "f" "sg")
    (mkLu "vuestro" "det" "pos" "m" "pl")
    (mkLu "vuestro" "det" "pos" "f" "pl") ;
  lin youPol_Pron  = mkNewPron GrammarSpa.youPol_Pron
    (mkLu "prpers" "prn" "tn" "p1" "mf" "sg") -- usted
    (mkLu "prpers" "prn" "tn" "p1" "mf" "sg") -- usted
    (mkLu "prpers" "prn" "pro" "p3" "m" "sg") -- lo
    (mkLu "prpers" "prn" "pro" "p3" "mf" "sg") -- le
    (mkLu "suyo" "det" "pos" "mf" "sg") -- su
    (mkLu "suyo" "det" "pos" "mf" "sg") -- su
    (mkLu "suyo" "det" "pos" "mf" "pl") -- sus
    (mkLu "suyo" "det" "pos" "mf" "pl") ; -- sus
  lin she_Pron     = mkNewPron GrammarSpa.she_Pron
    (mkLu "prpers" "prn" "tn" "p3" "f" "sg") -- ella
    (mkLu "prpers" "prn" "tn" "p3" "f" "sg") -- ella
    (mkLu "prpers" "prn" "pro" "p3" "f" "sg") -- la
    (mkLu "prpers" "prn" "pro" "p3" "mf" "sg") -- le
    (mkLu "suyo" "det" "pos" "mf" "sg") -- su
    (mkLu "suyo" "det" "pos" "mf" "sg") -- su
    (mkLu "suyo" "det" "pos" "mf" "pl") -- sus
    (mkLu "suyo" "det" "pos" "mf" "pl") ; -- sus
  lin he_Pron      = mkNewPron GrammarSpa.he_Pron
    (mkLu "prpers" "prn" "tn" "p3" "m" "sg") -- él
    (mkLu "prpers" "prn" "tn" "p3" "m" "sg") -- él
    (mkLu "prpers" "prn" "pro" "p3" "m" "sg") -- lo
    (mkLu "prpers" "prn" "pro" "p3" "mf" "sg") -- le
    (mkLu "suyo" "det" "pos" "mf" "sg") -- su
    (mkLu "suyo" "det" "pos" "mf" "sg") -- su
    (mkLu "suyo" "det" "pos" "mf" "pl") -- sus
    (mkLu "suyo" "det" "pos" "mf" "pl") ; -- sus
  lin it_Pron      = mkNewPron GrammarSpa.it_Pron
    (mkLu "prpers" "prn" "tn" "p3" "nt" "sg") -- él
    (mkLu "prpers" "prn" "tn" "p3" "nt" "sg") -- él
    (mkLu "prpers" "prn" "pro" "p3" "m" "sg") -- lo
    (mkLu "prpers" "prn" "pro" "p3" "mf" "sg") -- le
    (mkLu "suyo" "det" "pos" "mf" "sg") -- su
    (mkLu "suyo" "det" "pos" "mf" "sg") -- su
    (mkLu "suyo" "det" "pos" "mf" "pl") -- sus
    (mkLu "suyo" "det" "pos" "mf" "pl") ; -- sus
  lin we_Pron      = mkNewPron GrammarSpa.we_Pron
    (mkLu "prpers" "prn" "tn" "p1" "m" "pl") -- nosotros
    (mkLu "prpers" "prn" "tn" "p1" "m" "pl") -- nosotros
    (mkLu "prpers" "prn" "pro" "p1" "mf" "pl") -- nos
    (mkLu "prpers" "prn" "pro" "p1" "mf" "pl") -- nos
    (mkLu "nuestro" "det" "pos" "m" "sg") -- nuestro
    (mkLu "nuestro" "det" "pos" "f" "sg") -- nuestra
    (mkLu "nuestro" "det" "pos" "m" "pl") -- nuestros
    (mkLu "nuestro" "det" "pos" "f" "pl") ; -- nuestras
  lin they_Pron    = mkNewPron GrammarSpa.they_Pron
    (mkLu "prpers" "prn" "tn" "p3" "m" "pl") -- ellos
    (mkLu "prpers" "prn" "tn" "p3" "m" "pl") -- ellos
    (mkLu "prpers" "prn" "pro" "p3" "m" "pl") -- los
    (mkLu "prpers" "prn" "pro" "p3" "m" "pl") -- los
    (mkLu "suyo" "det" "pos" "mf" "sg") -- su
    (mkLu "suyo" "det" "pos" "mf" "sg") -- su
    (mkLu "suyo" "det" "pos" "mf" "pl") -- sus
    (mkLu "suyo" "det" "pos" "mf" "pl") ; -- sus

  lin
    aFeminineNoun = {
      s = \\n => case <n> of {
        <Sg> => mkLu "casa" "n" "f" "sg" ;
        <Pl> => mkLu "casa" "n" "f" "pl"
      } ;
      g = Fem } ;
    aMasculineNoun = {
      s = \\n => case <n> of {
        <Sg> => mkLu "coche" "n" "m" "sg" ;
        <Pl> => mkLu "coche" "n" "m" "pl"
      };
      g = Masc } ;

  -- Adjectives
  lin short_A =
      let cortas: Str = mkLu "corto" "adj" "f" "pl" ;
          corta : Str = mkLu "corto" "adj" "f" "sg";
          cortos: Str = mkLu "corto" "adj" "m" "pl";
          corto : Str = mkLu "corto" "adj" "m" "sg";
          cortamente: Str = mkLu "cortamente" "adv" ;
          mas      : Str = mkLu "más" "adv"
      in {
      s = \\degree,aform => case <degree,aform> of {
        < Posit, (AF Masc Sg) >   => corto ;
        < Posit, (AF Masc Pl) >   => cortos ;
        < Posit, (AF Fem Sg) >    => corta ;
        < Posit, (AF Fem Pl) >    => cortas ;
        < Posit, AA >             => cortamente ;
        < Compar, (AF Masc Sg) >  => mas ++ corto ;
        < Compar, (AF Masc Pl) >  => mas ++ cortos ;
        < Compar, (AF Fem Sg) >   => mas ++ corta ;
        < Compar, (AF Fem Pl) >   => mas ++ cortas ;
        < Compar, AA >            => mas ++ cortamente ;
        < Superl, (AF Masc Sg) >  => mas ++ corto ;
        < Superl, (AF Masc Pl) >  => mas ++ cortos ;
        < Superl, (AF Fem Sg) >   => mas ++ corta ;
        < Superl, (AF Fem Pl) >   => mas ++ cortas ;
        < Superl, AA >            => mas ++ cortamente
      } ;
      isPre = False
    } ;
  lin new_A =
      let nuevas : Str = mkLu "nuevo" "adj" "f" "pl" ;
          nueva  : Str = mkLu "nuevo" "adj" "f" "sg" ;
          nuevos : Str = mkLu "nuevo" "adj" "m" "pl" ;
          nuevo  : Str = mkLu "nuevo" "adj" "m" "sg" ;
          nuevamente : Str = mkLu "nuevamente" "adv" ;
          mas      : Str = mkLu "más" "adv"
      in {
      s = \\degree,aform => case <degree,aform> of {
        < Posit, (AF Masc Sg) >   => nuevo ;
        < Posit, (AF Masc Pl) >   => nuevos ;
        < Posit, (AF Fem Sg) >    => nueva ;
        < Posit, (AF Fem Pl) >    => nuevas ;
        < Posit, AA >             => nuevamente ;
        < Compar, (AF Masc Sg) >  => mas ++ nuevo ;
        < Compar, (AF Masc Pl) >  => mas ++ nuevos ;
        < Compar, (AF Fem Sg) >   => mas ++ nueva ;
        < Compar, (AF Fem Pl) >   => mas ++ nuevas ;
        < Compar, AA >            => mas ++ nuevamente ;
        < Superl, (AF Masc Sg) >  => mas ++ nuevo ;
        < Superl, (AF Masc Pl) >  => mas ++ nuevos ;
        < Superl, (AF Fem Sg) >   => mas ++ nueva  ;
        < Superl, (AF Fem Pl) >   => mas ++ nuevas ;
        < Superl, AA >            => mas ++ nuevamente
      } ;
      isPre = True
    } ;
  lin stupid_A =
      let estupidas : Str = mkLu "estúpido" "adj" "f" "pl" ;
          estupida  : Str = mkLu "estúpido" "adj" "f" "sg" ;
          estupidos : Str = mkLu "estúpido" "adj" "m" "pl" ;
          estupido  : Str = mkLu "estúpido" "adj" "m" "sg" ;
          estupidamente : Str = mkLu "estúpidamente" "adv" ;
          mas      : Str = mkLu "más" "adv"
      in {
      s = \\degree,aform => case <degree,aform> of {
        < Posit, (AF Masc Sg) >   => estupido  ;
        < Posit, (AF Masc Pl) >   => estupidos ;
        < Posit, (AF Fem Sg) >    => estupida ;
        < Posit, (AF Fem Pl) >    => estupidas ;
        < Posit, AA >             => estupidamente ;
        < Compar, (AF Masc Sg) >  => mas ++ estupido ;
        < Compar, (AF Masc Pl) >  => mas ++ estupidos ;
        < Compar, (AF Fem Sg) >   => mas ++ estupida ;
        < Compar, (AF Fem Pl) >   => mas ++ estupidas ;
        < Compar, AA >            => mas ++ estupidamente ;
        < Superl, (AF Masc Sg) >  => mas ++ estupido ;
        < Superl, (AF Masc Pl) >  => mas ++ estupidos ;
        < Superl, (AF Fem Sg) >   => mas ++ estupida ;
        < Superl, (AF Fem Pl) >   => mas ++ estupidas ;
        < Superl, AA >            => mas ++ estupidamente
      } ;
      isPre = True
    } ;
--     -- anAdjective = {
--     --   s = \\degree,aform => case <degree,aform> of {
--     --     < Posit, (AF Masc Sg) >   => mkLu "bueno" "adj" "m" "sg" ; -- bueno
--     --     < Posit, (AF Masc Pl) >   => mkLu "bueno" "adj" "m" "pl" ; -- buenos
--     --     < Posit, (AF Fem Sg) >    => mkLu "bueno" "adj" "f" "sg" ; -- buena
--     --     < Posit, (AF Fem Pl) >    => mkLu "bueno" "adj" "f" "pl" ; -- buenas
--     --     < Posit, AA >             => mkLu "ibuenamente" "adv"    ; -- buenamente
--     --     < Compar, (AF Masc Sg) >  => mkLu "mejor" "adj" "mf" "sg" ; -- mejor
--     --     < Compar, (AF Masc Pl) >  => mkLu "mejor" "adj" "mf" "pl" ; -- mejores
--     --     < Compar, (AF Fem Sg) >   => mkLu "mejor" "adj" "mf" "sg" ; -- mejor
--     --     < Compar, (AF Fem Pl) >   => mkLu "mejor" "adj" "mf" "pl" ; -- mejores
--     --     < Compar, AA >            => mkLu "mejormente" "adv"  ; -- mejormente
--     --     < Superl, (AF Masc Sg) >  => mkLu "mejor" "adj" "mf" "sg" ; -- mejor
--     --     < Superl, (AF Masc Pl) >  => mkLu "mejor" "adj" "mf" "pl" ; -- mejores
--     --     < Superl, (AF Fem Sg) >   => mkLu "mejor" "adj" "mf" "sg" ; -- mejor
--     --     < Superl, (AF Fem Pl) >   => mkLu "mejor" "adj" "mf" "pl" ; -- mejores
--     --     < Superl, AA >            => mkLu "mejormente" "adv" -- mejormente
--     --   } ;
--     --   isPre = True
--     -- } ;
-- 
  -- ~~~ IndefArt ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  oper indefiniteArticle : Number => Gender => Case => Str
    = \\n,g,c => case <n,g,c> of {
      <Sg,Masc,Nom>           => un ;
      <Sg,Masc,Acc>           => un ;
      <Sg,Masc,(CPrep P_de)>  => de_pr ++ un ; -- del
      <Sg,Masc,(CPrep P_a)>   => a_pr  ++ un ; -- al
      <Sg,Fem,Nom>            => una ;
      <Sg,Fem,Acc>            => una ;
      <Sg,Fem,(CPrep P_de)>   => de_pr ++ una ;
      <Sg,Fem,(CPrep P_a)>    => a_pr  ++ una ;
      <Pl,Masc,Nom>           => "" ;
      <Pl,Masc,Acc>           => "" ;
      <Pl,Masc,(CPrep P_de)>  => de_pr ;
      <Pl,Masc,(CPrep P_a)>   => a_pr ;
      <Pl,Fem,Nom>            => "" ;
      <Pl,Fem,Acc>            => "" ;
      <Pl,Fem,(CPrep P_de)>   => de_pr ;
      <Pl,Fem,(CPrep P_a)>    => a_pr }
    where { un  : Str = mkLu "uno" "det" "ind" "m" "sg" ;
            una : Str = mkLu "uno" "det" "ind" "f" "sg" ;
            de  : Str = de_pr ;
            a   : Str = a_pr } ;

  lin IndefArt = {
      s = table {
        True => \\_,_ => table {
          Nom => "" ;
          Acc => "" ;
          CPrep P_de => de_pr ;
          CPrep P_a  => a_pr
          } ;
        False => indefiniteArticle } ;
      s2 = "" ;
      sp = indefiniteArticle ;
      isNeg = False } ;

  -- ~~~ DefArt ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin DefArt = {
    s = \\_ => definiteArticle ;
    s2 = "" ;
    sp = definiteArticle ;
    isNeg = False } ;

   oper definiteArticle : Number => Gender => Case => Str
      = \\n,g,c => case <n,g,c> of {
        <Sg,Masc,Nom>           => el ;
        <Sg,Masc,Acc>           => el ;
        <Sg,Masc,(CPrep P_de)>  => de ++ el ; -- del
        <Sg,Masc,(CPrep P_a)>   => a ++ el ; -- al
        <Sg,Fem,Nom>            => la ;
        <Sg,Fem,Acc>            => la ;
        <Sg,Fem,(CPrep P_de)>   => de ++ la ;
        <Sg,Fem,(CPrep P_a)>    => a ++la ;
        <Pl,Masc,Nom>           => los ;
        <Pl,Masc,Acc>           => los ;
        <Pl,Masc,(CPrep P_de)>  => de ++ los ;
        <Pl,Masc,(CPrep P_a)>   => a ++ los ;
        <Pl,Fem,Nom>            => las ;
        <Pl,Fem,Acc>            => las ;
        <Pl,Fem,(CPrep P_de)>   => de ++ las ;
        <Pl,Fem,(CPrep P_a)>    => a ++ las }
      where { el  : Str = mkLu "el" "det" "def" "m" "sg" ;
              la  : Str = mkLu "el" "det" "def" "f" "sg" ;
              los : Str = mkLu "el" "det" "def" "m" "pl" ;
              las : Str = mkLu "el" "det" "def" "f" "pl" ;
              de  : Str = de_pr ;
              a   : Str = a_pr } ;

  -- ~~~ SlashV2a ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin SlashV2a v = mkVPSlash v.c2 (ExampleGeneratorSpa.predV v) ;
  oper predV : Verb -> VP = \verb ->
    let typ = verb.vtyp ;
    in {
      s = {s = verb.s ; vtyp = typ} ;
      agr    = partAgr typ ;
      neg    = ExampleGeneratorSpa.negation ;
      clit1  = [] ;
      clit2  = [] ;
      clit3  = {s,imp = [] ; hasClit = False} ;  --- refl is treated elsewhere
      isNeg  = False ; 
      comp   = \\a => [] ;
      ext    = \\p => []
    };

  oper negation : RPolarity => (Str * Str) = table {
    RPos => <[],[]> ;
    RNeg _ => <mkLu "no" "adv",[]>
  } ;

  -- ~~~ in_Prep ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin in_Prep = mkNewPrep GrammarSpa.in_Prep (pr "en") ;

  -- ~~~ and_Conj ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin and_Conj = mkNewConj GrammarSpa.and_Conj
    [] (mkLu "y" "cnjcoo") ; -- y

  -- ~~~ for_Prep ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin for_Prep = mkNewPrep GrammarSpa.for_Prep (pr "para") ;

  -- ~~~ to_Prep ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  lin to_Prep = mkNewPrep GrammarSpa.to_Prep "" ;
  -- *** Utils *************************************************************
  -- functions to make overwriting stuff easier

  -- Conj
  oper mkNewConj : Conj -> Str -> Str -> Conj
    = \conj,si,entonces -> lin Conj { s1 = si ; s2 = entonces ; n = conj.n } ;
  -- Prep
  oper mkNewPrep : Prep -> Str -> Prep
    = \prep,s -> lin Prep
      { s = s ; c = prep.c ; isDir = prep.isDir } ;

  -- 
  lin ProgrVP vp = 
     insertComplement 
       (\\agr => 
          let 
            clpr = <vp.clit1,vp.clit2> ; ----e pronArg agr.n agr.p vp.clAcc vp.clDat ;
            obj  = clpr.p2 ++ vp.comp ! agr ++ vp.ext ! RPos ---- pol
          in
          vp.s.s ! VGer ++ clpr.p1 ++ obj
       )
       (predV (BeschSpa.verbBesch estar ** {vtyp = VHabere ; lock_V = <>}));


}
