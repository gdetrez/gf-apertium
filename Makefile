
## Apentium install dir
APERTIUM=/usr/local/share/apertium
CAT = Utt
DEPTH = 4

all: apertium-en-es.norules.bin apertium-en-es.gfrules.bin


gfo:
	@echo "Create a directory to hold the gfo files"
	mkdir $@

ExampleGenerator.pgf: $(wildcard *.gf) | gfo
	@echo "Creating the pgf file"
	gf --make --gfo-dir=gfo ExampleGenerator.gf ExampleGeneratorEng.gf ExampleGeneratorSpa.gf

aligned-examples.gz: ExampleGenerator.pgf
	@echo "Generating aligned examples from the grammar"
	runghc generate_alignments.hs \
	  --depth=$(DEPTH) \
	  --cat=$(CAT) \
	  $< | gzip > $@

aligned-examples-big.gz: ExampleGenerator.pgf
	@echo "Generating aligned examples from the grammar (Depth 5)"
	runghc generate_alignments.hs \
	  --depth=5 \
	  --cat=$(CAT) \
	  $< | gzip > $@


alignment-templates.gz: aligned-examples.gz apertium-en-es.transfer-at.en-es.atx
	@echo "Extracing alignment templates from examples"
	apertium-validate-transfer-at apertium-en-es.transfer-at.en-es.atx
	apertium-xtract-alignment-templates \
	  -x apertium-en-es.transfer-at.en-es.atx \
	  -b ${APERTIUM}/apertium-en-es/en-es.autobil.bin \
	  -i $< \
	  -o $@ -e --noword4word \
	  --gzip

alignment-templates.uniq.gz: alignment-templates.gz
	@echo "Unifying alignment templates from file alignment-templates..."
	zcat alignment-templates | ./filter_alignment_templates.awk |\
	  cut -d' ' -f1 --complement | sort | uniq -c | sed -re "s/^[ ]+//g" |\
	  sort -t '|' -k 2,2 | gzip > $@

apertium-en-es.gfrules.tlx: alignment-templates.uniq.gz
	@echo "Converting alignment templates from $< into apertium transfer rules..."
	apertium-gen-transfer-from-aligment-templates \
	  -i $< \
	  --gzip \
	  -m 1 > $@

## Rule that preprocess apertium transfer rules
%.bin: %.tlx
	apertium-preprocess-transfer $< $@

clean:
	-rm apertium-en-es.norules.bin
	-rm apertium-en-es.gfrules.bin
	-rm apertium-en-es.gfrules.tlx
	-rm aligned-examples
	-rm ExampleGenerator.pgf
	-rm *.gfo
	-rm gfo/*
	-rm _tmpi _tmpo
	-rm alignment-templates alignment-templates.uniq
