--# -path=.:../abstract:../common:../../prelude

--1 English auxiliary operations.

-- This module contains operations that are needed to make the
-- resource syntax work. To define everything that is needed to
-- implement $Test$, it moreover contains regular lexical
-- patterns needed for $Lex$.

resource ResEng = ParamX ** open Prelude, ApertiumStreamFormat in {

  flags optimize=all ;


-- Some parameters, such as $Number$, are inherited from $ParamX$.

--2 For $Noun$

-- This is case as needed when inflecting nouns.

  param
    Case = Nom | Gen ;

-- This is the worst-case $Case$ needed for pronouns: I, me, my, mine

    NPCase = NCase Case | NPAcc | NPNomPoss ;

-- Useful macros and conversions:

  oper
    npNom : NPCase = NCase Nom ;
    npGen : NPCase = NCase Gen ;

    npcase2case : NPCase -> Case = \nc -> case nc of {NCase c => c ; _ => Nom} ;

-- Agreement of $NP$ has 8 values. $Gender$ is needed for "who"/"which" and
-- for "himself"/"herself"/"itself".

  param
    Agr = AgP1 Number | AgP2 Number | AgP3Sg Gender | AgP3Pl ;

  param 
    Gender = Neutr | Masc | Fem ;


--2 For $Verb$

-- Only these five forms are needed for open-lexicon verbs.

  param
    VForm = 
       VInf
     | VPres
     | VPPart
     | VPresPart
     | VPast      --# notpresent
     ;

-- Auxiliary verbs have special negative forms.

    VVForm = 
       VVF VForm
     | VVPresNeg
     | VVPastNeg  --# notpresent
     ;

-- The order of sentence is needed already in $VP$.

    Order = ODir | OQuest ;

-- The type of complement of a VV

    VVType = VVAux | VVInf | VVPresPart ; -- can do / try to do / start doing

--2 For $Adjective$

    AForm = AAdj Degree Case | AAdv ;

--2 For $Relative$
 
    RAgr = RNoAg | RAg Agr ;
    RCase = RPrep Gender | RC Gender NPCase ;

--2 For $Numeral$

    CardOrd = NCard | NOrd ;
    DForm = unit | teen | ten  ;

--2 Transformations between parameter types

  oper
    toAgr : Number -> Person -> Gender -> Agr = \n,p,g -> 
      case p of {
        P1 => AgP1 n ;
        P2 => AgP2 n ;
        P3 => case n of {
          Sg => AgP3Sg g ;
          Pl => AgP3Pl
          }
        } ;

    fromAgr : Agr -> {n : Number ; p : Person ; g : Gender} = \a -> case a of {
      AgP1 n => {n = n ; p = P1 ; g = Masc} ;
      AgP2 n => {n = n ; p = P2 ; g = Masc} ;
      AgP3Pl => {n = Pl ; p = P3 ; g = Masc} ;
      AgP3Sg g => {n = Sg ; p = P3 ; g = g}
      } ;

    agrP3 : Number -> Agr = \n -> agrgP3 n Neutr ;

    agrgP3 : Number -> Gender -> Agr = \n,g -> toAgr n P3 g ;

    conjAgr : Agr -> Agr -> Agr = \a0,b0 -> 
      let a = fromAgr a0 ; b = fromAgr b0 
      in
      toAgr
        (conjNumber a.n b.n)
        (conjPerson a.p b.p) a.g ;

-- For $Lex$.

-- For each lexical category, here are the worst-case constructors.

    mkNoun : (_,_,_,_ : Str) -> {s : Number => Case => Str} = 
      \man,mans,men,mens -> {
      s = table {
        Sg => table {
          Gen => mans ;
          _ => man
          } ;
        Pl => table {
          Gen => mens ;
          _ => men
          }
        }
      } ;

    mkAdjective : (_,_,_,_ : Str) -> {s : AForm => Str; lock_A : {}} = 
      \good,better,best,well -> lin A {
      s = table {
        AAdj Posit  c => (regGenitiveS good) ! c ;
        AAdj Compar c => (regGenitiveS better) ! c ;
        AAdj Superl c => (regGenitiveS best) ! c ;
        AAdv          => well
        }
      } ;

    mkVerb : (_,_,_,_,_ : Str) -> Verb = 
      \go,goes,went,gone,going -> {
      s = table {
        VInf   => go ;
        VPres  => goes ;
        VPast  => went ; --# notpresent
        VPPart => gone ;
        VPresPart => going
        } ;
      p = [] ; -- no particle
      isRefl = False
      } ;

    mkIP : (i,me,my : Str) -> Number -> {s : NPCase => Str ; n : Number} =
     \i,me,my,n -> let who = mkNP i me my n P3 Neutr in {
        s = who.s ; 
        n = n
        } ;

    mkNP : (i,me,my : Str) -> Number -> Person -> Gender -> 
     {s : NPCase => Str ; a : Agr} = \i,me,my,n,p,g -> 
   { s = table {
       NCase Nom => i ;
       NPAcc => me ;
       NCase Gen | NPNomPoss => my -- works for normal genitives, "whose", etc.
       } ;
     a = toAgr n p g ;
   };

    regNP : Str -> Number -> {s : NPCase => Str ; a : Agr} = \that,n ->
      mkNP that that (that + "'s") n P3 Neutr ;

    regGenitiveS : Str -> Case => Str = \s -> 
      table { Gen => genitiveS s; _ => s } ;

    genitiveS : Str -> Str = \dog ->
      case last dog of {
          "s" => dog + "'" ;
          _   => dog + "'s"
          };

-- We have just a heuristic definition of the indefinite article.
-- There are lots of exceptions: consonantic "e" ("euphemism"), consonantic
-- "o" ("one-sided"), vocalic "u" ("umbrella").

    artIndef = pre {
      "eu" | "Eu" | "uni" | "up" => "a" ;
      "un" => "an" ; 
      "a" | "e" | "i" | "o" | "A" | "E" | "I" | "O" => "an" ;
      "SMS" | "sms" => "an" ; ---
      _ => "a"
      } ;

    artDef = "the" ;

-- For $Verb$.

  Verb : Type = {
    s : VForm => Str ;
    p : Str ; -- verb particle
    isRefl : Bool
    } ;

  param
  CPolarity = 
     CPos
   | CNeg Bool ;  -- contracted or not

  oper
  contrNeg : Bool -> Polarity -> CPolarity = \b,p -> case p of {
    Pos => CPos ;
    Neg => CNeg b
    } ;

  VerbForms : Type =
    Tense => Anteriority => CPolarity => Order => Agr => 
      {aux, adv, fin, inf : Str} ; -- would, not, sleeps, slept

  VP : Type = {
    s   : VerbForms ;
    p   : Str ;   -- verb particle
    prp : Str ;   -- present participle 
    ptp : Str ;   -- past participle
    inf : Str ;   -- the infinitive form ; VerbForms would be the logical place
    ad  : Str ;   -- sentence adverb
    s2  : Agr => Str -- complement
    } ;


  SlashVP = VP ** {c2 : Str ; gapInMiddle : Bool} ;

  predVc : (Verb ** {c2 : Str}) -> SlashVP = \verb -> 
    predV verb ** {c2 = verb.c2 ; gapInMiddle = True} ;

  oper predV : Verb -> VP = \verb -> {
    s = \\t,ant,b,ord,agr => 
      let inf  = verb.s ! VInf ;
          fin  = presVerb verb agr ;
          part = verb.s ! VPPart ;
          did       : Str = mkLu "do" "vbdo" "past" ;
          didnt     : Str = did ++ mkLu "not" "adv";
          have_     : Str = mkLu "have" "vbhaver" "inf" ;
          had       : Str = mkLu "have" "vbhaver" "past" ;
          hadnt     : Str = had ++  mkLu "not" "adv";
          will      : Str = mkLu "will" "vaux" "inf" ;
          willnt    : Str = will ++  mkLu "not" "adv";
          would     : Str = mkLu "would" "vaux" "inf" ;
          wouldnt   : Str = would ++ mkLu "not" "adv";
      in case <t,ant,b,ord> of {
        <Pres,Simul,CPos,ODir>   => vff            fin [] ;
        <Pres,Simul,CPos,OQuest> => vf (does agr)   inf ;
        <Pres,Anter,CPos,_>      => vf (have agr)   part ;
        <Pres,Anter,CNeg c,_>    => vfn c (have agr) (havent agr) part ;
        <Past,Simul,CPos,ODir>   => vff (verb.s ! VPast) [] ;
        <Past,Simul,CPos,OQuest> => vf did inf ;
        <Past,Simul,CNeg c,_>    => vfn c did didnt inf ;
        <Past,Anter,CPos,_>      => vf  had part ;
        <Past,Anter,CNeg c,_>    => vfn c had hadnt part ;
        <Fut, Simul,CPos,_>      => vf will inf ;
        <Fut, Simul,CNeg c,_>    => vfn c will willnt inf ;
        <Fut, Anter,CPos,_>      => vf will       (have_ ++ part) ;
        <Fut, Anter,CNeg c,_>    => vfn c will willnt (have_ ++ part) ;
        <Cond,Simul,CPos,_>      => vf would inf ;
        <Cond,Simul,CNeg c,_>    => vfn c would wouldnt inf ;
        <Cond,Anter,CPos,_>      => vf would (have_ ++ part);
        <Cond,Anter,CNeg c,_>    => vfn c would wouldnt (have_ ++ part);
        <Pres,Simul,CNeg c,_>    => vfn c (does agr) (doesnt agr) inf
      } ;
    p    = verb.p ;
    prp  = verb.s ! VPresPart ;
    ptp  = verb.s ! VPPart ;
    inf  = verb.s ! VInf ;
    ad   = [] ;
    s2 = \\a => if_then_Str verb.isRefl (reflPron ! a) []
  } ;

  
  oper predAux : Aux -> VP = \verb -> {
    s = \\t,ant,cb,ord,agr =>
      let b = case cb of { CPos => Pos ; _ => Neg } ;
          inf   = verb.inf ;
          fin   = verb.pres ! b ! agr ;
          finp  = verb.pres ! Pos ! agr ;
          part  = verb.ppart ;
          have_     : Str = mkLu "have" "vbhaver" "inf" ;
          had       : Str = mkLu "have" "vbhaver" "past" ;
          hadnt     : Str = had   ++ not;
          will      : Str = mkLu "will" "vaux" "inf" ;
          wont      : Str = will  ++ not;
          would     : Str = mkLu "would" "vaux" "inf" ;
          wouldnt   : Str = would ++ not;
      in
        case <t,ant,cb,ord> of {
          <Pres,Anter,CPos,_>      => vf    (have agr)   part ;
          <Pres,Anter,CNeg c,_>    => vfn c (have agr) (havent agr) part ;
          <Past,Simul,CPos,  _>    => vf    (verb.past ! b ! agr) [] ;
          <Past,Simul,CNeg c,  _>  => vfn c (verb.past!Pos!agr)(verb.past!Neg!agr) [] ;
          <Past,Anter,CPos,_>      => vf    had         part ; 
          <Past,Anter,CNeg c,_>    => vfn c had   hadnt part ;
          <Fut, Simul,CPos,_>      => vf    will        inf ;
          <Fut, Simul,CNeg c,_>    => vfn c will  wont      inf ;
          <Fut, Anter,CPos,_>      => vf    will       (have_ ++ part) ;
          <Fut, Anter,CNeg c,_>    => vfn c will  wont(have_ ++ part) ;
          <Cond,Simul,CPos,_>      => vf    would      inf ;
          <Cond,Simul,CNeg c,_>    => vfn c would wouldnt   inf ;
          <Cond,Anter,CPos,_>      => vf    would      (have_ ++ part) ;
          <Cond,Anter,CNeg c,_>    => vfn c would wouldnt (have_ ++ part) ;
          <Pres,Simul,CPos,  _>    => vf    fin          [] ;
          <Pres,Simul,CNeg c,  _>  => vfn c finp  fin          []
        } ;
      p = [] ;
      prp = verb.prpart ;
      ptp = verb.ppart ;
      inf = verb.inf ;
      ad = [] ;
      s2 = \\_ => []
  } ;

  vff : Str -> Str -> {aux, adv, fin, inf : Str} = \x,y -> 
    {aux = [] ; adv = [] ; fin = x ; inf = y} ;

  vf : Str -> Str -> {aux, adv, fin, inf : Str} = \x,y -> vfn True x x y ;

  oper vfn : Bool -> Str -> Str -> Str -> {aux, fin, adv, inf : Str} =
    \contr,x,y,z -> case contr of {
      True  => {aux = y ; adv = []                ; fin = [] ; inf = z} ;
      False => {aux = x ; adv = mkLu "not" "adv"  ; fin = [] ; inf = z}
    } ;

  insertObj : (Agr => Str) -> VP -> VP = \obj,vp -> {
    s = vp.s ;
    p = vp.p ;
    prp = vp.prp ;
    ptp = vp.ptp ;
    inf = vp.inf ;
    ad = vp.ad ;
    s2 = \\a => vp.s2 ! a ++ obj ! a
    } ;

  insertObjPre : (Agr => Str) -> VP -> VP = \obj,vp -> {
    s = vp.s ;
    p = vp.p ;
    prp = vp.prp ;
    ptp = vp.ptp ;
    inf = vp.inf ;
    ad = vp.ad ;
    s2 = \\a => obj ! a ++ vp.s2 ! a 
    } ;

  insertObjc : (Agr => Str) -> SlashVP -> SlashVP = \obj,vp -> 
    insertObj obj vp ** {c2 = vp.c2 ; gapInMiddle = vp.gapInMiddle} ;

--- AR 7/3/2013 move the particle after the object
  insertObjPartLast : (Agr => Str) -> VP -> VP = \obj,vp -> {
    s = vp.s ;
    p = [] ;  -- remove particle from here
    prp = vp.prp ;
    ptp = vp.ptp ;
    inf = vp.inf ;
    ad = vp.ad ;
    s2 = \\a => obj ! a ++ vp.s2 ! a ++ vp.p  -- and put it here ; corresponds to insertObjPre
    } ;

--- The adverb should be before the finite verb.

  insertAdV : Str -> VP -> VP = \ad,vp -> {
    s = vp.s ;
    p = vp.p ;
    prp = vp.prp ;
    ptp = vp.ptp ;
    inf = vp.inf ;
    ad  = vp.ad ++ ad ;
    s2 = \\a => vp.s2 ! a
    } ;

-- 

  predVV : {s : VVForm => Str ; p : Str ; typ : VVType} -> VP = \verb ->
    let verbs = verb.s
    in
    case verb.typ of {
      VVAux => predAux {
        pres = table {
          Pos => \\_ => verbs ! VVF VPres ;
          Neg => \\_ => verbs ! VVPresNeg
          } ;
        past = table {                       --# notpresent
          Pos => \\_ => verbs ! VVF VPast ;  --# notpresent
          Neg => \\_ => verbs ! VVPastNeg    --# notpresent
          } ;    --# notpresent
        p = verb.p ;
        inf = verbs ! VVF VInf ;
        ppart = verbs ! VVF VPPart ;
        prpart = verbs ! VVF VPresPart ;
        } ;
      _    => predV {s = \\vf => verbs ! VVF vf ; p = verb.p ; isRefl = False}
      } ;

  presVerb : {s : VForm => Str} -> Agr -> Str = \verb -> 
    agrVerb (verb.s ! VPres) (verb.s ! VInf) ;

  infVP : VVType -> VP -> Anteriority -> CPolarity -> Agr -> Str
    = \typ,vp,ant,cb,a ->
      let have   : Str = mkLu "have" "vbhaver" "inf" ;
          having : Str = mkLu "have" "vbhaver" "ger" ;
          to     : Str = mkLu "to" "pr" in
    case cb of {CPos => ""; _ => not} ++
    case ant of {
      Simul => case typ of {
                 VVAux => vp.ad ++ vp.inf ; 
                 VVInf => to ++ vp.ad ++ vp.inf ;
                 _ => vp.ad ++ vp.prp
               }
      ; Anter => case typ of {                                    --# notpresent
                 VVAux => have ++ vp.ad ++ vp.ptp ;                --# notpresent
                 VVInf => to ++ have ++ vp.ad ++ vp.ptp ;        --# notpresent
                 _     => having ++ vp.ad ++ vp.ptp                 --# notpresent
               }                                                       --# notpresent
    } ++ vp.p ++
    vp.s2 ! a ;

  agrVerb : Str -> Str -> Agr -> Str = \has,have,agr -> 
    case agr of {
      AgP3Sg _ => has ;
      _        => have
      } ;

  -- Overrides opers `have`, `havent`, `does`, `doesnt`
  -- NOTE: for both of those, abertium has two dictconary entries:
  -- one marked <vblex> and one with a special tag (<vbhave> for have and
  -- <vbdo> for do)
  -- I am assuming that we want the special tag in this case
  oper have : Agr -> Str = \a ->
    case a of {
      AgP3Sg _  => mkLu "have" "vbhaver" "pri" "p3" "sg" ; -- has
      _         => mkLu "have" "vbhaver" "pres" -- have
    } ;
  oper havent : Agr -> Str = \a -> have a ++ not ;
  oper does : Agr -> Str = \a ->
    case a of {
      AgP3Sg _  => mkLu "do" "vbdo" "pri" "p3" "sg" ; -- does
      _         => mkLu "do" "vbdo" "pres" -- do
    } ;
  oper doesnt : Agr -> Str = \a -> does a ++ not ;
  oper not : Str = mkLu "not" "adv" ;

  Aux = {
    pres : Polarity => Agr => Str ; 
    past : Polarity => Agr => Str ;  --# notpresent
    inf,ppart,prpart : Str
    } ;

  oper auxBe : Aux = {
    pres = \\b,a => posneg b (case a of {
      AgP1 Sg   => mkLu "be" "vbser" "pri" "p1" "sg" ;
      AgP3Sg _  => mkLu "be" "vbser" "pri" "p3" "sg" ;
      _         => mkLu "be" "vbser" "pres" 
    }) ;
    past = \\b,a => posneg b (case a of {          --# notpresent
      AgP1 Sg  => mkLu "be" "vbser" "past" "p1" "sg" ; --# notpresent
      AgP3Sg _ => mkLu "be" "vbser" "past" "p3" "sg" ;
      _        => mkLu "be" "vbser" "past"
      }) ;
    inf  = mkLu "be" "vbser" "inf" ;
    ppart = mkLu "be" "vbser" "pp" ;
    prpart = mkLu "be" "vbser" "pprs"
    } ;

  posneg : Polarity -> Str -> Str = \p,s -> case p of {
    Pos => s ;
    Neg => s + not
    } ;

  conjThat : Str = "that" ;

  -- override oper defining a reflexive pronoun
  oper reflPron : Agr => Str = table {
      AgP1 Sg      => mkLu "myself"     "prn" "ref" "p1" "mf" "sg" ; -- myself
      AgP2 Sg      => mkLu "yourself"   "prn" "ref" "p2" "mf" "sg" ; -- yourself
      AgP3Sg Masc  => mkLu "himself"    "prn" "ref" "p3" "m"  "sg" ; -- himself
      AgP3Sg Fem   => mkLu "herself"    "prn" "ref" "p3" "f"  "sg" ; -- herself
      AgP3Sg Neutr => mkLu "itself"     "prn" "ref" "p3" "nt" "sg" ; -- itself
      AgP1 Pl      => mkLu "ourselves"  "prn" "ref" "p1" "mf" "pl" ; -- ourselves
      AgP2 Pl      => mkLu "yourselves" "prn" "ref" "p2" "mf" "pl" ; -- yourselves
      AgP3Pl       => mkLu "themselves" "prn" "ref" "p3" "mf" "pl"   -- themselves
    } ;

-- For $Sentence$.

  Clause : Type = {
    s : Tense => Anteriority => CPolarity => Order => Str
    } ;

  mkClause : Str -> Agr -> VP -> Clause =
    \subj,agr,vp -> {
      s = \\t,a,b,o => 
        let 
          verb  = vp.s ! t ! a ! b ! o ! agr ;
          compl = vp.s2 ! agr
        in
        case o of {
          ODir => subj ++ verb.aux ++ verb.adv ++ vp.ad ++ verb.fin ++ verb.inf ++ vp.p ++ compl ;
          OQuest => verb.aux ++ subj ++ verb.adv ++ vp.ad ++ verb.fin ++ verb.inf ++ vp.p ++ compl
          }
    } ;


-- For $Numeral$.

  mkNum : Str -> Str -> Str -> Str -> {s : DForm => CardOrd => Case => Str} = 
    \two, twelve, twenty, second ->
    {s = table {
       unit => table {NCard => regGenitiveS two ; NOrd => regGenitiveS second} ; 
       teen => \\c => mkCard c twelve ; 
       ten  => \\c => mkCard c twenty
       }
    } ;

  regNum : Str -> {s : DForm => CardOrd => Case => Str} = 
    \six -> mkNum six (six + "teen") (six + "ty") (regOrd six) ;

  regCardOrd : Str -> {s : CardOrd => Case => Str} = \ten ->
    {s = table {NCard => regGenitiveS ten ; 
		NOrd => regGenitiveS (regOrd ten)} } ;

  mkCard : CardOrd -> Str -> Case => Str = \o,ten -> 
    (regCardOrd ten).s ! o ; 

  regOrd : Str -> Str = \ten -> 
    case last ten of {
      "y" => init ten + "ieth" ;
      _   => ten + "th"
      } ;

  mkQuestion : 
      {s : Str} -> Clause -> 
      {s : Tense => Anteriority => CPolarity => QForm => Str} = \wh,cl ->
      {
      s = \\t,a,p => 
            let 
              cls = cl.s ! t ! a ! p ;
              why = wh.s
            in table {
              QDir   => why ++ cls ! OQuest ;
              QIndir => why ++ cls ! ODir
              }
      } ;


}
