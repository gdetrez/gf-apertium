#!/usr/bin/make -f
## Evaluatin makefile
#
# This Makefile performs the evaluation of the gf/apertium experiment.
# It will translate the same test corpus with the different systems and
# compute bleu and nist score on them.
#
# It generates with names like $SYSTEM:{bleu,nist} which are property files
# that can be plotted uning jenkins
#
#
## Systems
SYSTEMS=apertium-en-es apertium-en-es-norules apertium-en-es-gfrules
## Corpora
CORPORA=newstest2011 newstest2011A newstest2011B
## Apentium install dir
APERTIUM=/usr/local/share/apertium



all: $(foreach s, $(SYSTEMS), $(foreach c, $(CORPORA), $c.$s.bleu))

## ~~~ System specific rules ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Those rules describe the translation process for each system.

## SYSTEM 0: No rules
%.apertium-en-es-norules.es.txt: %.en.txt
	@echo "NoRules: Translating newstest2011.en..."
	cat $< | apertium-destxt | \
	  lt-proc ${APERTIUM}/apertium-en-es/en-es.automorf.bin | \
	  apertium-tagger -g ${APERTIUM}/apertium-en-es/en-es.prob | \
	  apertium-pretransfer | \
	  apertium-transfer apertium-en-es.norules.tlx apertium-en-es.norules.bin ${APERTIUM}/apertium-en-es/en-es.autobil.bin | \
	  lt-proc -g ${APERTIUM}/apertium-en-es/en-es.autogen.bin | \
	  lt-proc -p ${APERTIUM}/apertium-en-es/en-es.autopgen.bin | \
	  apertium-retxt > $@

## SYSTEM 1: Default apertium en-es mode
%.apertium-en-es.es.txt: %.en.txt
	@echo "Apertium: Translating newstest2011.en..."
	cat $< | apertium en-es > $@

## SYSTEM 2: GF generated rules
%.apertium-en-es-gfrules.es.txt: %.en.txt
	@echo "GF rules: Translating newstest2011.en..."
	cat $< | apertium-destxt | \
	  lt-proc ${APERTIUM}/apertium-en-es/en-es.automorf.bin | \
	  apertium-tagger -g ${APERTIUM}/apertium-en-es/en-es.prob | \
	  apertium-pretransfer | \
	  apertium-transfer apertium-en-es.gfrules.tlx apertium-en-es.gfrules.bin ${APERTIUM}/apertium-en-es/en-es.autobil.bin | \
	  lt-proc -g ${APERTIUM}/apertium-en-es/en-es.autogen.bin | \
	  lt-proc -p ${APERTIUM}/apertium-en-es/en-es.autopgen.bin | \
	  apertium-retxt > $@

## ~~~ Evaluation rule ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# This is a geveric rule that, given the name of a system, perform the
# evaluation and generates property files with the results
# macro that returns the name of the corpus
corpus=$(firstword $(subst ., ,$1))
%.mteval: %.es.txt
	./mteval-v11b-nosgm.sh \
	  $(call corpus,$*).en.txt $(call corpus,$*).es.txt $< \
	  > $@


%.bleu: %.mteval
	cat $< | awk '/^NIST/ { print "YVALUE="$$8 > "$@" }'


%.nist: %.mteval
	cat $< | awk '/^NIST/ { print "YVALUE="$$4 > "$@" }'


clean:
	-rm $(patsubst %,%.es.txt,$(EVALUATIONS))
	-rm *.nist *.bleu
