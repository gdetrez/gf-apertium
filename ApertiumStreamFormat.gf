resource ApertiumStreamFormat = {

  -- Overloaded function that, given a lemma and a, potentially empty list
  -- of tags, generates a lexical unit in the form
  --     ^lemma<tag1><tag2>$
  oper mkLu = overload {
    mkLu : (lemma: Str) -> Str
      = \lemma -> "^" + lemma + "$" ;
    mkLu : (lemma: Str) -> (tag: Str) -> Str
      = \l,t -> "^" + l + tag t + "$" ;
    mkLu : (lemma: Str) -> (tag1, tag2: Str) -> Str
      = \l,t1,t2 -> "^" + l + tag t1 + tag t2 + "$";
    mkLu : (lemma: Str) -> (tag1, tag2, tag3: Str) -> Str
      = \l,t1,t2,t3 -> "^" + l + tag t1 + tag t2 + tag t3 + "$" ;
    mkLu : (lemma: Str) -> (tag1, tag2, tag3, tag4: Str) -> Str
      = \l,t1,t2,t3,t4 -> "^" + l + tag t1 + tag t2 + tag t3 + tag t4 + "$" ;
    mkLu : (lemma: Str) -> (tag1, tag2, tag3, tag4, tag5: Str) -> Str
      = \l,t1,t2,t3,t4,t5 ->
        "^" + l + tag t1 + tag t2 + tag t3 + tag t4 + tag t5 + "$" ;
    mkLu : (lemma: Str) -> (tag1, tag2, tag3, tag4, tag5, tag6: Str) -> Str
      = \l,t1,t2,t3,t4,t5,t6 ->
        "^" + l + tag t1 + tag t2 + tag t3 + tag t4 + tag t5 + tag t6 + "$" ;
  };

  oper tag : Str -> Str = \t -> "<" + t + ">";

}
-- concrete ExampleGeneratorSpa of ExampleGenerator =
